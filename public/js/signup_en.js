
  var minka_api_url = "https://dev.minka.io";
  var x_access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJQZXJtaXNzaW9ucyI6eyJlbmRwb2ludHMiOlsidHJhbnNmZXIiLCJwZXJzb24iLCJsdWthdGFnIiwib3JnIiwiaXRlbSIsImNvbnRyYWN0IiwiZ2F0ZXdheSJdLCJ2ZXJicyI6WyJQT1NUIiwiR0VUIiwiUFVUIiwiREVMRVRFIl19LCJpYXQiOjE1MDk3NTkxOTV9.sPybisuo2thYbHWNgQ0BS5EGIPG5FrnFZgeN3czwEWs";

  var phone = "";
  var lukatag = "";
  var fbid = "";
  var fbpageid = "";

  function GetQueryStringParams(sParam) {

        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
  }

  function init() {

    $('#prefix').dropdown();
    $('#country').dropdown();

    $('#phone_block').transition('hide');
    $('#btn_pin').transition('hide');
    $('#pin_block').transition('hide');
    $('#lukatag_block').transition('hide');
    $('#email_block').transition('hide');
    $('#profile_block').transition('hide');
    $('#success_block').transition('hide');
    $('#body').css('display', '');
    $('#phone_block').transition('scale');
    $('#btn_phone').on("click", verifyPhone);
    $('#btn_pin').on("click", verifyPIN);
    $('#btn_lukatag').on("click", verifyLukatag);
    $('#btn_email').on("click", verifyEmail);
    $('#btn_profile').on("click", verifyProfile);
    $('#btn_accept_terms').on("click", acceptTerms);
    $('#btn_hide_terms').on("click", hideTerms);

    $('.menu .item')
      .tab()
    ;

    $('#mainForm').unbind('submit');
    $('#btn_phone').api({
        beforeSend: function(settings) {
            return false;
        }
    });
    $('#btn_pin').api({
        beforeSend: function(settings) {
            return false;
        }
    });
    $('#btn_lukatag').api({
        beforeSend: function(settings) {
            return false;
        }
    });
    $('#btn_email').api({
        beforeSend: function(settings) {
            return false;
        }
    });
    $('#btn_profile').api({
        beforeSend: function(settings) {
            return false;
        }
    });

    fbid = GetQueryStringParams("fbid");
    if (fbid == undefined) fbid = "";
    fbpageid = GetQueryStringParams("fbpageid");
    if (fbpageid == undefined) fbpageid = "";

  }


  function verifyPhone() {

        $('#mainForm').form({
                  fields:
                  {
                    identifier:'phone_suffix',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Phone cannot be empty'
                        },
                        {
                          type   : 'number',
                          prompt : 'Phone number contains only numbers'
                        },
                        {
                          type   : 'regExp[/^[0-9]{8,10}$/]',
                          prompt : 'Phone number must be 8 to 10 digits'
                        }
                    ]
                  }

                },
                {
                    onSuccess: function(event) {
                      event.preventDefault();
                        //check phone

                        phone = $('#prefix').dropdown('get text').replace("+","") + $('#phone_suffix').val();
                        var url = minka_api_url + "/person/" + phone;

                        headerParams = {'x-access-token':x_access_token};
                        obj = {
                          type: 'get',
                          url: url,
                          headers: headerParams,
                          data: [],
                          dataType: 'json',
                          processData: false,
                          success: function(data) {
                            if (data == undefined) {
                              //new user
                              //reset error form
                              $('#error_block').html('<div class="ui error message"></div>');

                              //send SMS
                              url_pin = minka_api_url + "/channel/sms/pin"
                              obj_pin = {
                                type: 'get',
                                url: url,
                                headers: headerParams,
                                data: {"phone": phone},
                                dataType: 'json',
                                processData: false
                              }
                              $.ajax(obj_pin);

                              //show next step
                              setTimeout(function(){
                                $('#btn_phone').transition('hide');
                                $('#pin_block').transition('show');
                                $('#btn_pin').transition('show');
                              },400);
                            }
                            else if (data._id != undefined){
                              //exisiting user
                              $('#error_block').html('<div class="ui error message" style="display:block"><ul class="list"><li>This mobile phone is already registered</li></ul></div>');
                            }
                            else if (data.message != undefined) {
                              //error
                              $('#error_block').html('<li>' + data.message + '</li>');
                            }
                          }
                        };
                        $.ajax(obj);
                    },
                    onFailure: function(event) {
                      event.preventDefault();

                    }
                });

      }


  function verifyPIN() {

    $('#mainForm').form({
              fields:
              {
                identifier:'pin',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'PIN cannot be empty'
                    }
                ]
              }

            },
            {
                onSuccess: function(event) {

                  event.preventDefault();
                  $('#phone_block').transition('scale');
                  setTimeout(function(){
                    $('#lukatag_block').transition('scale');
                  },400);
                },
                onFailure: function(event) {
                  event.preventDefault();

                }
            });

    }


  function verifyLukatag() {

    $('#mainForm').form({
              fields:
              {
                identifier:'lukatag',
                rules: [
                    {
                        type   : 'empty',
                        prompt : '@lukatag cannot be empty'
                    },
                    {
                        type   : 'regExp[/^[a-zA-Z0-9_-]{3,25}$/]',
                        prompt : '@lukatag must contain 3 to 25 letters, digits or symbols _ and -.'
                    }
                ]
              }}
              ,{
                onSuccess: function(event) {
                  event.preventDefault();

                  //check lukatag

                  lukatag = "@" + $('#lukatag').val();
                  lukatag = lukatag.toLowerCase();

                  var url = minka_api_url + "/person/" + lukatag;
                  headerParams = {'x-access-token':x_access_token};
                  obj = {
                    type: 'get',
                    url: url,
                    headers: headerParams,
                    data: [],
                    dataType: 'json',
                    processData: false,
                    success: function(data) {
                      if (data == undefined) {
                        //new user
                        //reset error form
                        $('#error_block').html('<div class="ui error message"></div>');

                        //show next step
                        $('#lukatag_block').transition('scale');
                        setTimeout(function(){
                          $('#email_block').transition('scale');
                        },400);
                      }
                      else if (data._id != undefined){
                        //exisiting user
                        $('#error_block').html('<div class="ui error message" style="display:block"><ul class="list"><li>This @lukatag is already registered</li></ul></div>');
                      }
                      else if (data.message != undefined) {
                        //error
                        $('#error_block').html('<li>' + data.message + '</li>');
                      }
                    }
                  };
                  $.ajax(obj);

                },
                onFailure: function(event) {
                  event.preventDefault();

                }
            });


  }

  function verifyEmail() {
    $('#mainForm').form({
              fields:
              {
                identifier:'email',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Your e-mail cannot be empty'
                    },
                    {
                        type   : 'email',
                        prompt : 'Be careufl to type a valid e-mail address'
                    }
                ]
              }

            },
            {
                onSuccess: function(event) {
                  event.preventDefault();

                  //check lukatag

                  var url = minka_api_url + "/person/" + $('#email').val();
                  headerParams = {'x-access-token':x_access_token};
                  obj = {
                    type: 'get',
                    url: url,
                    headers: headerParams,
                    data: [],
                    dataType: 'json',
                    processData: false,
                    success: function(data) {
                      if (data == undefined) {
                        //new user
                        //reset error form
                        $('#error_block').html('<div class="ui error message"></div>');

                        //show next step
                        $('#email_block').transition('scale');
                        setTimeout(function(){
                          $('#profile_block').transition('scale');
                        },400);
                      }
                      else if (data._id != undefined){
                        //exisiting user
                        $('#error_block').html('<div class="ui error message" style="display:block"><ul class="list"><li>This e-mail is already registered</li></ul></div>');
                      }
                      else if (data.message != undefined) {
                        //error
                        $('#error_block').html('<li>' + data.message + '</li>');
                      }
                    }
                  };
                  $.ajax(obj);

                },
                onFailure: function(event) {
                  event.preventDefault();

                }
            });


  }

  function verifyProfile() {


    $('#mainForm').form(
              {
              fields:
                {
                  first_name: {
                  identifier:'first_name',
                  rules: [
                      {
                          type   : 'empty',
                          prompt : 'First name cannot be empty'
                      },
                      {
                        type   : 'regExp[/^[a-zA-Z ]{2,25}$/]',
                        prompt : 'First name must contain at least 2 characters, other symbols are not allowed'
                      }
                    ]
                  },
                  last_name: {
                    identifier:'last_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Last name cannot be empty'
                        },
                        {
                          type   : 'regExp[/^[a-zA-Z ]{2,25}$/]',
                          prompt : 'Last name must contain at least 2 characters, other symbols are not allowed'
                        }
                    ]
                  },
                  country: {
                    identifier:'country',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Country cannot be empty'
                        }
                    ]
                  },
                  city: {
                    identifier:'city',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'City cannot be empty'
                        },
                        {
                          type   : 'regExp[/^[a-zA-Z ]{2,25}$/]',
                          prompt : 'City must contain at least 2 characters, other symbols are not allowed'
                        }
                    ]
                  },
                  terms: {
                    identifier:'terms',
                    rules: [
                        {
                            type   : 'checked',
                            prompt : 'You must accept Terms and agreements to register'
                        }
                    ]
                  },
                }
              ,
              onSuccess: function(event) {
                event.preventDefault();

                //register profile

                var country = $('#country').dropdown('get text');
                var country_code = $('#country').dropdown('get value');
                var url = minka_api_url + "/person";

                headerParams = {'x-access-token':x_access_token};
                person_data = {data:{
                    isVerified: "true",
                    avatar: "http://avatar.com",
                    gender: "male",
                    lukatag: lukatag,
                    identification: [],
                    terms: null,
                    contacts: [
                        {
                            metadata: {
                                fbid: fbid
                            },
                            isVerified: "true",
                            value: fbpageid,
                            type: "facebook"
                        },
                        {
                            isVerified: "true",
                            value: phone,
                            type: "phone"
                        },
                        {
                            isVerified: "true",
                            value: $('#email').val(),
                            type: "email"
                        }
                    ],
                    timeCreated: new Date().getTime(),
                    locations: [
                        {
                            name: "default",
                            planet: "Earth",
                            city: $('#city').val(),
                            country: country,
                            countryCode: country_code,
                            region: $('#city').val(),
                            state: $('#city').val(),
                            street: $('#address').val(),
                            zip: "0",
                            timeZone: "",
                            utcOffset: "",
                            longitude: "",
                            latitude: "",
                            isVerified: "true",
                            timeCreated: new Date().getTime()
                        }
                    ],
                    name:
                        {
                            full: $('#first_name').val() + ' ' + $('#last_name').val(),
                            last: $('#last_name').val(),
                            first: $('#first_name').val()
                        }

                }};
                obj_person = {
                  type: 'POST',
                  url: url,
                  headers: headerParams,
                  data: person_data,
                  dataType: 'json',
                  success: function(result) {
                    if (result._id != undefined){
                      //user saved
                      //show next step
                      $('#profile_block').transition('scale');
                      $('#show_lukatag').text(lukatag);
                      setTimeout(function(){
                        $('#success_block').transition('scale');
                      },400);
                    }
                    else if (result.message != undefined) {
                      //error
                      $('#error_block').html('<li>' + result.message + '</li>');
                    }
                  }
                };
                $.ajax(obj_person);

              },
              onFailure: function(event) {
                event.preventDefault();
                setTimeout(function(){
                  $('#dimmer_block').dimmer('show');
                },400);
              }
            });

  }

  function showTerms() {
    $('#terms_block').modal('show');
  }

  function acceptTerms() {
    $('#accept_terms').checkbox('check');
    $('#terms_block').modal('hide');
  }

  function hideTerms() {
    $('#terms_block').modal('hide');
  }



//init
  $(document).ready(function() { init() });
