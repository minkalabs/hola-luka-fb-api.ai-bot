'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.fbid = session.fbid;
        this.luka = session.context.luka;

    }
    //main method
    execute(callback) {

      return ({
        //speech: msg,
        //displayText: msg,
        source: 'facebook',
        followupEvent: {
          name: 'person-balance-response',
          data: {
            balance: 'hello world!'
          }
        }
      });

    }
}
module.exports = ActionObject;
