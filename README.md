# "Hola Luka" API.AI Chat Bot
[![](http://minka.io/assets/img/MinkaMedium-logo.png)](www.holaluka.com "Get your own Hola Luka account")

## Chat bot based on Api.ai integration

#### Version: 1.0
#### Author: Tomislav Bišćan, tomi@minka.io
#### Date: July, 2017.

This is the main workflow describing how bot components communicate between each other. Semantic logic of the bot and natural language processing (NLP) is defined on api.ai level. User input is mapped to an intent in api.ai. Depending on intent and a current context, api.ai either responses directly or triggers Minka API to get response data. Basically there are 2 types of Minka API responses:

1. response containing structured data that is forwarded to the user as-is. Typically this is the type of response containing cards payload.

2. response containing data that is included in localized response from api.ai. Typically this is raw data response, for example balance data, "120 LUK". This raw data response is enriched in api.ai in a localized natural language reposnse, "Your balance is 120 LUK".

![](http://holaluka-fb-apiai-bot-dev.herokuapp.com/img/hola-luka-bot-workflow.png)

## Deploy and connect with Facebook

Deployement of "Hola Luka" API.AI Chat Bot includes following steps:

### 1. Creating a API.AI bot

You need to create an account and a new bot on [https://api.ai](https://api.ai). Import a bot using .zip file in **/api.ai_bot** project folder. **IMPORTANT:** api.ai bot supports only one language per bot. Once set it cannot be changed. To implement multi-language bot you need to create multiple bots, each for one language. To localize bots you have 2 options:

1. export bot from api.ai, unzip it and use text editor to localize all texts 

2. import generic bot in Spanish from **/api.ai_bot** and localize it using api.ai interface. This approach is better because you have a freedom to change texts and remove/add some phrases and responses.

### 2. creating a Facebook page and app

In this step you need to create a Facebook page, Facebook app and add Messenger integration. Follow [these instructions](https://docs.api.ai/docs/facebook-integration) for this step.

### 3. deploying "Hola Luka" API.AI Chat Bot nodeJS app

```bash

mkdir <path_to_local_repo>
cd <path_to_local_repo>
git init
git pull https://bitbucket.org/minkalabs/hola-luka-fb-api.ai-bot.git

```

Configuration parameters are located in app.json. Mandatory parameters are: 

APIAI_ACCESS_TOKEN (Client access token for Api.ai), 
APIAI_LANG (Agent language), 
FB_PAGE_ACCESS_TOKEN (Page Access Token), 
FB_VERIFY_TOKEN (verification code), 
MINKA_ACCESS_TOKEN (Minka client access token), 
MINKA_API_URL (Minka API URL), 
BOT_APP_URL (Chat Bot Web Application URL).


```bash

sudo nano app.json
npm start

```
## Session handling
api.ai and bot clients (Facebook, Telegram...) don't share the same session space. Since we need to have a personalized bot experience for a user, these session spaces need to be connected. In this version of bot we use NodeJS app session to hold a data for every user. Downside is that restarting and application destroys the session. **TO DO: Redis or similar store needs to be implemented as a session store**.

Session is created/restored at the point when a user writes something to the bot. At that point we have information about user id for a specific bot (i.e. sender_id for Facebook). Part of code that handles session is a part of *processEvent(event)* function.

```bash

// Handle a text message from this sender
if (!this.sessionIds.has(sender)) {
    this.sessionIds.set(sender, uuid.v4());
}
sessionId = this.sessionIds.get(sender);

//handle bot session for every user
//findOrCreateSession
if (sessions[sessionId] == undefined) {
    sessions[sessionId] = {sessionId: sessionId, fbid: sender, fbpageid: receiver, is_admin: true, context: {}};
    sessions[sessionId].context.luka = new Luka();
    sessions[sessionId].context.luka.sender_id = sender;
    sessions[sessionId].context.luka.bot = this;
}

```
Notice that a new instance of class Luka is created for every session. Class Luka is an instance of *minka_api.js*. It is a container for user specific parameters as well as Minka API interface. This can be optimized by extending *session* object with all parameters held in *minka_api_js*. **TO DO: optimze session object with parameters from minka_api.js**.

At the point where api.ai executes Minka API action via webhook, session is loaded using session_id using following code.

```bash

//get session
session = sessions[sessionId];

```

### Linking accounts

One level of session usage is recognizing the context and the profile of the bot platform (i.e. Facebook user). Another level is connecting this user to it's "Hola Luka" profile. This is handled during the "login" intent. Once a user is recognized and verified using SMS PIN code, it's "Hola Luka" profile is extended with a new Channel. Depending on the source (Facebook, Telegram..) different channel is added/updated with combination of source page-user id (i.e. Facebook page user ID) and a page id. This is handled in action *src/actions/person.login.verify.pin.js*.

```bash

//set authenticated flag
me.luka.authenticated = true;
//add a channel to person profile
var channel = {data: { type: me.source, name: me.source, id: me.session.fbpageid, status: "VERIFIED", metadata: { fbid: me.session.fbpageid}}}
me.luka.genericPost(`POST`,`/channel/${me.parameters.lukatag}`,channel, function(result){
  if (result.response == false) {
    return reject('error');
  }
  else {
    callback({
      source: `${me.source}`,
      followupEvent: {
        name: `person-login-confirm`,
        data: { phone: me.luka.profile.phone, lukatag: me.parameters.lukatag}
      }
    });

  }

```

## Calling a Minka API
To call Minka API methods and endpoints you can use either:

1. pre-built Minka API calls defined in *minka_api.js* i.e. **getBalance(lukatag, callback)**

2. generic Minka API POST method call in *minka_api.js* **genericPost(method, uri, body, callback)**

All methods should be called in Promises and handling callbacks.

## Implementing webhooks and actions

To understand the concept of api.ai workflow, intents, contexts and webhooks please make sure you have studied these articles:

[Basics](https://api.ai/docs/getting-started/basics)

[Intents](https://api.ai/docs/intents)

[Contexts](https://api.ai/docs/contexts)

[Events](https://api.ai/docs/events)

[Actions](https://api.ai/docs/actions-and-parameters)

[Fulfillment](https://api.ai/docs/fulfillment)

So, to summerize:

**Intents** enable bot to recognize users action or a command to be executed. They are triggered either by *User says* recognizing the input text or by calling events.

**Contexts** act as a dialogue session. They help you hold a value for certain number of user inputs.

**Events** act as a triggers to execute an intent or api.ai action. When you want to make a call from a bot to api.ai you call an event, for example *person-login-verify-pin*. In most cases they act as a follow up events, executing after some webhook code is executed.

```bash
//ask user to enter pin
callback({
  source: `${me.source}`,
  followupEvent: {
    name: `person-login-verify-pin`,
    data: { phone: profile.phone, lukatag: profile.lukatag }
  }
}
```
**Actions** are the custom code or Minka API functions you call after some intent is fullfiled. Intent being fullfield means that all mandatory parameters are collected. 

### Actions naming convention

Naming the action and custom NodeJS function is crucial for function execution. The name of the action and the name of the NodeJS holding the code needs to be exactly the same, with addition of .js extension for NodeJS custom code file.

For example to implement action **persion.pin.verify** you need to create file **/src/actions/person.pin.verify.js**. This action will be executed upon fullfilment and all generic data will be initialized and passed as a reference. Code implementation for that mechanism can be found in *src/app.js*.

```bash
//API.AI object
const action = JSON.parse(req.body).result.action;
const actionIncomplete = JSON.parse(req.body).result.actionIncomplete;
var parameters = JSON.parse(req.body).result.parameters;
var contexts = JSON.parse(req.body).result.contexts;
const intentName = JSON.parse(req.body).result.intentName;
var sessionId = JSON.parse(req.body).sessionId;
var source = ''; //Facebook, Kik, Telegram...

//find source (Facebook, Kik, Telegram...)
switch (facebookBot.apiAiService.requestSource) {
    case "fb":
        source = "facebook";
    break;
}

//get session
session = sessions[sessionId];

//initiate action object
const ActionObject = require(`./actions/${action}.js`);
const actionObject = new ActionObject(actionIncomplete, parameters, contexts, intentName, session, source);

//execute default action and forward response
const result = actionObject.execute(function(response){
  console.log(`webhook return: ${JSON.stringify(response)}`);  
  return res.json(response);
})    
```

### Creating new actions
Action object needs to follow mentioned naming convention, needs to be placed in *src/actions* directory and needs to have at least following interface.

```bash
'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;

    }

    //main method
    execute(callback) {

      return ({
        //speech: msg,
        //displayText: msg,
        source: 'facebook',
        followupEvent: {
          name: 'name-event',
          data: {
            param: 'hello world!'
          }
        }
      });

    }
}
module.exports = ActionObject;
```

### Anonymous and personalized actions
Some actions can be triggered for anonymous users. If you want to check and force a user to login to consume your action you can do it easily by adding this snippet of code on the top of *execute(callback)* method.

```bash
var me = this;
//check if user is authenticated
if (!me.luka.authenticated) {
callback({
  source: `${this.source}`,
  followupEvent: {
    name: 'not-authenticated',
    data: {}
  }
});
}

```

### Admin actions
If a user has **is_admin** flag in session set tu **true** then it has a admin level access. You can use to implement methods that are available only for this level of access. For example intent **admin balance @lukatag** will return a balance for any user. This is example of an action that is not available for all users. To check is user is admin you can simply add this snippet of code on the top of *execute(callback)* method.

```bash
var me = this;
//check if user is authenticated and if it is admin
if (!me.luka.authenticated && !me.session.is_admin) {
callback({
  source: `${this.source}`,
  followupEvent: {
    name: 'input.unknown',
    data: {}
  }
});
}
```
If user is doesn't have a admin level access it's user input will be recognized as *input.unknown* and the bot will respond as a to any unrecognized input or command.

## Extending bot workflows
To design a new bot workflow you first need to think of all the steps a user will go through and detect the points where:

1. which user input will trigger the intent? For example "pay bill 12345"

2. which user parameters you need to collect? For example "bill_id"

3. which user parameters need to be validated? To implement a parameter validation you have 2 strategies. One is to use Webook slot filling and to call the same action upon every user input. Another is to split intent workflow with a new step. See example for *Person.Register* or *Person.Login* intents.

4. which action will be executed? Name the action and describe the return data. Make sure you don't put any locale specific text in actions. Rather add another intent step and fill it with parameters returned. See *Person.Balance* and *Person.Balance.Response* intents as an example.

5. what is the fallback event? Think of and define fallback event in case invalid user input or an error. 

After the diagram is designed, use api.ai static text response to test workflow. After that let developer implement actions to get real data.

### _Note about languages_
You need to provide language parameter according to your agent settings in the form of two-letters code.
 
 * "en"
 * "ru"
 * "de"
 * "pt"
 * "pt-BR"
 * "es"
 * "fr"
 * "it"
 * "ja"
 * "ko"
 * "zh-CN"
 * "zh-HK"
 * "zh-TW"
