const apiai = require('../src/apiai/index.js');
var util = require('util');
const APIAI_TOKEN = "bb33a3cf597b4ce1bab56ca91ff8b916";
var apiaiApp = apiai(APIAI_TOKEN, {language: 'es'});
var apiaiTextRequest;
var apiaiDeleteContextsRequest;



var event = {
    name: 'service-pay-verify',
    data: {}
};

var options = {
    sessionId: 1
};

Promise.resolve(true)
  // 1. Receive "foo" concatenate "bar" to it and resolve that to the next then
  .then(function() {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        apiaiTextRequest = apiaiApp.textRequest(`cancelar`, {sessionId: 1});
        apiaiTextRequestAddEvents(apiaiTextRequest);
        resolve(apiaiTextRequest);
      }, 1500);
    });
  })
  // 2. receive "foobar", register a callback function to work on that string
  // and print it to the console, but not before return the unworked on
  // string to the next then
  .then(function() {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        apiaiTextRequest = apiaiApp.textRequest(`hola`, {sessionId: 1});
        apiaiTextRequestAddEvents(apiaiTextRequest);
        resolve(apiaiTextRequest);
      },1500);
    });
  })
  // 3. print helpful messages about how the code in this section will be run
  // before string is actually processed by the mocked asynchronous code in the
  // prior then block.
  .then(function() {
    return new Promise(function(resolve, reject){
      setTimeout(function(apiaiTextRequest){
        apiaiTextRequest = apiaiApp.eventRequest(event, options);
        apiaiTextRequestAddEvents(apiaiTextRequest);
        resolve(apiaiTextRequest);
      },1500)
    })

  });

  function apiaiTextRequestAddEvents(apiaiTextRequest){

    apiaiTextRequest.on('response', function(response) {

      //console.log(util.inspect(response, false, null));

      //console.log(`apiai: ${response.result.resolvedQuery} ${response.result.action} ${response.result.actionIncomplete} ${response.result.metadata.intentName} ${JSON.stringify(response.result.parameters)} ${JSON.stringify(response.result.contexts)} ${response.result.fulfillment.messages[0].speech}`);
      //console.log(`apiai response: ${JSON.stringify(response)}`);
      //print all messages
      for (var nResponses = 0; nResponses < response.result.fulfillment.messages.length; nResponses++) {
        var apiResponseText = response.result.fulfillment.messages[nResponses].speech;
        //if api.ai recognized intent, return api.ai response
        //console.log(`apiai response: ${JSON.stringify(response)}`);
        console.log(`apiai textresponse: ${apiResponseText}`);
      }

    });

    apiaiTextRequest.on('error', function(error) {
        console.log(error);
    });

    apiaiTextRequest.end();
  }

/*

Promise.resolve(apiaiTextRequest)
  .then(function(apiaiTextRequest){return apiaiApp.textRequest(`cancelar`, {sessionId: 1})})
  .then(console.log('1'))
  .then(function(){return apiaiApp.eventRequest(event, options)})
  .then(console.log('2'));
*/
  //.then(apiaiTextRequest = apiaiApp.eventRequest(event, options));


//apiaiTextRequest = apiaiApp.eventRequest(event, options);
