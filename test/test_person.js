const _ = require('underscore')
    var profile = `[
    {
        "_id": "5963e0110c5a75469e00088a",
        "passcode": "$2a$10$BmkFQX3VLDI/oVO4dYuXXuEaG4F1rZkimd307m6XqVNlP/xl.4qB6",
        "email": "andres@minka.io",
        "phone": "573004431529",
        "lukatag": "@andres",
        "lastname": "Rengifo",
        "firstname": "Andres",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e0110c5a75469e00088f",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e0110c5a75469e00088e"
            }
        ],
        "location": [
            {
                "city": "Bgota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e0110c5a75469e00088d"
            }
        ],
        "identification": [
            {
                "numberId": "012345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e0110c5a75469e00088b",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e0110c5a75469e00088c"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e07e0c5a75469e000898",
        "passcode": "$2a$10$3WRCpQ46XqFDw.IA9nf0Delu.6S49N/1eUj5EBw.W9vskeKHwXfs6",
        "email": "dom@minka.io",
        "phone": "5711234567",
        "lukatag": "@dom",
        "lastname": "Rozic",
        "firstname": "Domagoj",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e07e0c5a75469e00089d",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e07e0c5a75469e00089c"
            },
            {
                "_id": "5981f70bc9b76b3b8bcd0419"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e07e0c5a75469e00089b"
            }
        ],
        "identification": [
            {
                "numberId": "212345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e07e0c5a75469e000899",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e07e0c5a75469e00089a"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e0a90c5a75469e00089f",
        "passcode": "$2a$10$J8CyGBWVlmTSEOicCxIpRuYk62u4DPuMNv7KliIQTrN4/utGsxG9O",
        "email": "suport@minka.io",
        "phone": "5721234567",
        "lukatag": "@luka",
        "lastname": "luka",
        "firstname": "minka",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e0a90c5a75469e0008a4",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e0a90c5a75469e0008a3"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e0a90c5a75469e0008a2"
            }
        ],
        "identification": [
            {
                "numberId": "312345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e0a90c5a75469e0008a0",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e0a90c5a75469e0008a1"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e0e40c5a75469e0008a6",
        "passcode": "$2a$10$rj5.gC1aQp6xq3J9AkSjyOJZowlry8R54jwp5FdUFT.7cp3h0fzte",
        "email": "paola@minka.io",
        "phone": "5731234567",
        "lukatag": "@paola",
        "lastname": "Sanchez",
        "firstname": "Paola",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e0e40c5a75469e0008ab",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e0e40c5a75469e0008aa"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e0e40c5a75469e0008a9"
            }
        ],
        "identification": [
            {
                "numberId": "412345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e0e40c5a75469e0008a7",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e0e40c5a75469e0008a8"
                    }
                ]
            }
        ]
    },
    {
        "_id": "59795c9bd8a0702df6b89240",
        "passcode": "$2a$10$F/8ef/54c4Sxgq3ndG.bdeSBVl.xfawuZw0ovX9eI1EjctRBEHeQu",
        "email": "email",
        "phone": "57phone",
        "lukatag": "@lukataggggg",
        "lastname": "lastname",
        "firstname": "firstname",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "59795c9bd8a0702df6b89245",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "59795c9bd8a0702df6b89244"
            }
        ],
        "location": [
            {
                "city": "city",
                "country": "Colombia",
                "countryCode": "CO",
                "address": "address",
                "_id": "59795c9bd8a0702df6b89243"
            }
        ],
        "identification": [
            {
                "numberId": "numberId",
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.232Z",
                "verified": true,
                "_id": "59795c9bd8a0702df6b89241",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "59795c9bd8a0702df6b89242"
                    }
                ]
            }
        ]
    },
    {
        "_id": "598344843d4ce53236ccca7e",
        "passcode": "$2a$10$e/JTgypP/si8CDw3Kr0/uOFCHnSXsG1.5hmdKDvMcRvvfvsz1mHmG",
        "email": "damian@minka.io",
        "phone": "573185951061",
        "lukatag": "@damian",
        "lastname": "fandiño",
        "firstname": "damian",
        "__v": 0,
        "created": "August 3rd 2017, 11:42:36 am",
        "terms": [
            {
                "channel": "postman",
                "verified": true,
                "_id": "598344843d4ce53236ccca83",
                "dateConfirmed": "August 3rd 2017, 11:42:36 am",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "postman",
                "name": "postman",
                "id": "postman",
                "status": "verified",
                "_id": "598344843d4ce53236ccca82"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "address": "calle falsa 1,2,3",
                "state": "Cundinamarca",
                "_id": "598344843d4ce53236ccca81"
            }
        ],
        "identification": [
            {
                "numberId": "1018449252",
                "typeId": "CC",
                "dateIssue": "2010-03-01T05:00:00.000Z",
                "verified": true,
                "_id": "598344843d4ce53236ccca7f",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "598344843d4ce53236ccca80"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e15b0c5a75469e0008b4",
        "passcode": "$2a$10$jACk/Hf5YxnJnkMgyhrKmucWHCXbCTuciqJYygReC4NKEUWVxb4IO",
        "email": "carolina@minka.io",
        "phone": "5751234567",
        "lukatag": "@carolina",
        "lastname": "Gomez",
        "firstname": "Carolina",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e15b0c5a75469e0008b9",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e15b0c5a75469e0008b8"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e15b0c5a75469e0008b7"
            }
        ],
        "identification": [
            {
                "numberId": "612345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e15b0c5a75469e0008b5",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e15b0c5a75469e0008b6"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e17f0c5a75469e0008bb",
        "passcode": "$2a$10$rYs.4xVaf/a4bKMveZfZLeYTac8mtqAcRe8C1OwirbrmGoCKufk8C",
        "email": "juan@minka.io",
        "phone": "5761234567",
        "lukatag": "@juan",
        "lastname": "Gomez",
        "firstname": "Juan",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e17f0c5a75469e0008c0",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e17f0c5a75469e0008bf"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e17f0c5a75469e0008be"
            }
        ],
        "identification": [
            {
                "numberId": "712345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e17f0c5a75469e0008bc",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e17f0c5a75469e0008bd"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e1b60c5a75469e0008c2",
        "passcode": "$2a$10$85GW4pHOrwhykAtGjKwNC.NtCr80d1V8X4dJLUfBxDavdlTwK40lO",
        "email": "lucia@minka.io",
        "phone": "5771234567",
        "lukatag": "@lucia",
        "lastname": "Moreno",
        "firstname": "Lucia",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e1b60c5a75469e0008c7",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e1b60c5a75469e0008c6"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e1b60c5a75469e0008c5"
            }
        ],
        "identification": [
            {
                "numberId": "812345678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e1b60c5a75469e0008c3",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e1b60c5a75469e0008c4"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e43b0c5a75469e0008ca",
        "passcode": "$2a$10$yNNAV2/9X0D/gfJoyre1HOLApcg2XFavFgDCoE8dWAqTBXymPLuge",
        "email": "test$$$_ @minka.io",
        "phone": "57321456",
        "lukatag": "@test",
        "lastname": "test",
        "firstname": "test",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e43b0c5a75469e0008cf",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e43b0c5a75469e0008ce"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "5963e43b0c5a75469e0008cd"
            }
        ],
        "identification": [
            {
                "numberId": "678",
                "typeId": "cc",
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "verified": true,
                "_id": "5963e43b0c5a75469e0008cb",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5963e43b0c5a75469e0008cc"
                    }
                ]
            }
        ]
    },
    {
        "_id": "59664f4b6b6ded4c3f942d82",
        "passcode": "$2a$10$Bu7CzFGjBzDEV7q9j3XdfeN8oFjPq6Tr2N7tIf7bewr8xprZOWvui",
        "email": "anthony@minka.io",
        "phone": "573505389482",
        "lukatag": "@conradical",
        "lastname": "Conrad",
        "firstname": "Anthony",
        "__v": 0,
        "created": "July 11th 2017, 10:26:18 pm",
        "terms": [
            {
                "channel": "web",
                "verified": true,
                "_id": "59664f4b6b6ded4c3f942d87",
                "dateConfirmed": "July 11th 2017, 10:26:18 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "web",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "59664f4b6b6ded4c3f942d86"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "_id": "59664f4b6b6ded4c3f942d85"
            }
        ],
        "identification": [
            {
                "numberId": "406632",
                "typeId": "CE",
                "dateIssue": "2016-10-18T00:00:00.000Z",
                "verified": true,
                "_id": "59664f4b6b6ded4c3f942d83",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "59664f4b6b6ded4c3f942d84"
                    }
                ]
            }
        ]
    },
    {
        "_id": "59669378c763f6497e0ea0b1",
        "passcode": "$2a$10$7CgU0b7qxoTNpduQZwVByusLhj4E3EWPvwH5NR6.p7fP8BrXwDCem",
        "email": "bellido.oliver@gmail.com",
        "phone": "59178888088",
        "lukatag": "@olib",
        "lastname": "Bellido",
        "firstname": "Oliver",
        "__v": 0,
        "created": "July 12th 2017, 12:56:55 pm",
        "terms": [
            {
                "channel": "web",
                "verified": true,
                "_id": "59669378c763f6497e0ea0b6",
                "dateConfirmed": "July 12th 2017, 12:56:55 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "web",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "59669378c763f6497e0ea0b5"
            }
        ],
        "location": [
            {
                "city": "Marte",
                "country": "Jupiter",
                "countryCode": "BO",
                "_id": "59669378c763f6497e0ea0b4"
            }
        ],
        "identification": [
            {
                "numberId": "43526123",
                "typeId": "CC",
                "dateIssue": "1986-11-10T05:00:00.000Z",
                "verified": true,
                "_id": "59669378c763f6497e0ea0b2",
                "locationIssue": [
                    {
                        "city": "LaPaz",
                        "country": "Bolivia",
                        "countryCode": "BO",
                        "_id": "59669378c763f6497e0ea0b3"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5966a54ac763f6497e0ea0c5",
        "passcode": "$2a$10$kJwVEfLBMSazTwbYlA3TcOHbkNvAgiTPRbT0fmIENhXrCDjNO0BV6",
        "email": "eduardoa@sintesis.com.bo",
        "phone": "59167894739",
        "lukatag": "@eduaranda",
        "lastname": "Aranda",
        "firstname": "Eduardo",
        "__v": 0,
        "created": "July 12th 2017, 12:56:55 pm",
        "terms": [
            {
                "channel": "web",
                "verified": true,
                "_id": "5966a54ac763f6497e0ea0ca",
                "dateConfirmed": "July 12th 2017, 12:56:55 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "web",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5966a54ac763f6497e0ea0c9"
            }
        ],
        "location": [
            {
                "city": "SantaCruz",
                "country": "Bolivia",
                "countryCode": "BO",
                "_id": "5966a54ac763f6497e0ea0c8"
            }
        ],
        "identification": [
            {
                "numberId": "3369737",
                "typeId": "CI",
                "dateIssue": "2018-10-06T04:00:00.000Z",
                "verified": true,
                "_id": "5966a54ac763f6497e0ea0c6",
                "locationIssue": [
                    {
                        "city": "LaPaz",
                        "country": "Bolivia",
                        "countryCode": "BO",
                        "_id": "5966a54ac763f6497e0ea0c7"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5968fd69c77d23587cfbfab1",
        "passcode": "$2a$10$UAfMxUdKRPMcNCdTR9LrCOi6LKhSOctcdivdXI7LBJpHm7UZHvP0q",
        "email": "marko.erkic@infobip.com",
        "phone": "573184155030",
        "lukatag": "@marko",
        "lastname": "Erkic",
        "firstname": "Marko",
        "__v": 0,
        "created": "July 13th 2017, 12:50:37 pm",
        "terms": [
            {
                "channel": "web",
                "verified": true,
                "_id": "5968fd69c77d23587cfbfab5",
                "dateConfirmed": "July 13th 2017, 12:50:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "web",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5968fd69c77d23587cfbfab4"
            }
        ],
        "location": [
            {
                "city": "bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "address": "Bogota",
                "_id": "5968fd69c77d23587cfbfab3"
            }
        ],
        "identification": [
            {
                "numberId": "406272",
                "typeId": "CE",
                "verified": true,
                "_id": "5968fd69c77d23587cfbfab2",
                "locationIssue": []
            }
        ]
    },
    {
        "_id": "59795f8bd8a0702df6b8926a",
        "passcode": "$2a$10$jMePa1tlpUJh31rJxSp81uI6yDzyJwDZQ2s4KnZ13WPfYEytBtl0.",
        "email": "sdf@tom.vi",
        "phone": "591234234",
        "lukatag": "@qawywsx",
        "lastname": "sdfsdf",
        "firstname": "sdf",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "59795f8bd8a0702df6b8926f",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "59795f8bd8a0702df6b8926e"
            }
        ],
        "location": [
            {
                "city": "bogota",
                "country": "Colombia",
                "countryCode": "BO",
                "address": "wefweklfjwlef 23",
                "_id": "59795f8bd8a0702df6b8926d"
            }
        ],
        "identification": [
            {
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.234Z",
                "verified": true,
                "_id": "59795f8bd8a0702df6b8926b",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "59795f8bd8a0702df6b8926c"
                    }
                ]
            }
        ]
    },
    {
        "_id": "59796004d8a0702df6b8928e",
        "passcode": "$2a$10$KJYZ6bcT6i6/JegMHWdPXuo7m7lrM4cfF/cuUyPI2ewpc/Ax9fXZa",
        "email": "sdf@t22om.vi",
        "phone": "59123234234",
        "lukatag": "@qa12wywsx",
        "lastname": "sdfsdf",
        "firstname": "sdf",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "59796004d8a0702df6b89293",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "59796004d8a0702df6b89292"
            }
        ],
        "location": [
            {
                "city": "bogota",
                "country": "Colombia",
                "countryCode": "BO",
                "address": "wefweklfjwlef 23",
                "_id": "59796004d8a0702df6b89291"
            }
        ],
        "identification": [
            {
                "numberId": "numb1erId",
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.234Z",
                "verified": true,
                "_id": "59796004d8a0702df6b8928f",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "59796004d8a0702df6b89290"
                    }
                ]
            }
        ]
    },
    {
        "_id": "597965f0d8a0702df6b8929c",
        "passcode": "$2a$10$UGi1VFOWRlgetbH4bF33c.fvisdQBAJLkVdDzt0E75UAMb9PHrhjO",
        "email": "tomi@minka.io",
        "phone": "591234234234",
        "lukatag": "@wedwed",
        "lastname": "wert",
        "firstname": "wert",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "597965f0d8a0702df6b892a1",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "597965f0d8a0702df6b892a0"
            }
        ],
        "location": [
            {
                "city": "bogota",
                "country": "Colombia",
                "countryCode": "BO",
                "address": "erwterb 23",
                "_id": "597965f0d8a0702df6b8929f"
            }
        ],
        "identification": [
            {
                "numberId": "1501128126943",
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.234Z",
                "verified": true,
                "_id": "597965f0d8a0702df6b8929d",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "597965f0d8a0702df6b8929e"
                    }
                ]
            }
        ]
    },
    {
        "_id": "59796b3cd8a0702df6b892b1",
        "passcode": "$2a$10$IPmZwzmi5S9iYv5WDLsHueyifUZpaEuz7d5bvwbzIbiDMpRXTrf6y",
        "email": "tom@rrrr.rtz",
        "phone": "59123424234234",
        "lukatag": "@wqawqa",
        "lastname": "232r23r",
        "firstname": "werwer",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "59796b3cd8a0702df6b892b6",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "59796b3cd8a0702df6b892b5"
            },
            {
                "_id": "59796b61d8a0702df6b892b7"
            }
        ],
        "location": [
            {
                "city": "bogota",
                "country": "Colombia",
                "countryCode": "BO",
                "address": "wjelkjwekr344",
                "_id": "59796b3cd8a0702df6b892b4"
            }
        ],
        "identification": [
            {
                "numberId": "1501129531962",
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.234Z",
                "verified": true,
                "_id": "59796b3cd8a0702df6b892b2",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "59796b3cd8a0702df6b892b3"
                    }
                ]
            }
        ]
    },
    {
        "_id": "59798cecd8a0702df6b892b9",
        "passcode": "$2a$10$OfqL81XM6.CV3awYe8JbBO6.VZS6IeEu0vLMmeeuJqE5WL.39lXUu",
        "email": "Gggg.jh@567.vg",
        "phone": "5915555865865",
        "lukatag": "@pacino",
        "lastname": "Uvug",
        "firstname": "Ugugugu",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "59798cecd8a0702df6b892be",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "59798cecd8a0702df6b892bd"
            }
        ],
        "location": [
            {
                "city": "Uhhhhhh 666",
                "country": "Colombia",
                "countryCode": "BO",
                "address": "Uhhhhhh 666",
                "_id": "59798cecd8a0702df6b892bc"
            }
        ],
        "identification": [
            {
                "numberId": "1501138155953",
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.234Z",
                "verified": true,
                "_id": "59798cecd8a0702df6b892ba",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "59798cecd8a0702df6b892bb"
                    }
                ]
            }
        ]
    },
    {
        "_id": "597f4fb2d8a0702df6b892e9",
        "passcode": "$2a$10$9FL8yzDV3DxXqifush2.tO9PO4vzvueNlik1DCaJ/gbd33Wt5q476",
        "email": "marydim0902@gmail.com",
        "phone": "5913164127670",
        "lukatag": "@mary",
        "lastname": "Diaz",
        "firstname": "Maritza",
        "__v": 0,
        "created": "July 25th 2017, 5:52:37 pm",
        "terms": [
            {
                "channel": "channel",
                "verified": true,
                "_id": "597f4fb2d8a0702df6b892ee",
                "dateConfirmed": "July 25th 2017, 5:52:37 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "typeChannel",
                "name": "nameChannel",
                "id": "idChannel",
                "status": "statusChannel",
                "_id": "597f4fb2d8a0702df6b892ed"
            },
            {
                "_id": "597f4fd8d8a0702df6b892ef"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "BO",
                "address": "Street 1",
                "_id": "597f4fb2d8a0702df6b892ec"
            }
        ],
        "identification": [
            {
                "numberId": "1501515697885",
                "typeId": "typeId",
                "dateIssue": "1970-01-01T00:00:01.234Z",
                "verified": true,
                "_id": "597f4fb2d8a0702df6b892ea",
                "locationIssue": [
                    {
                        "city": "city",
                        "country": "country",
                        "countryCode": "BO",
                        "_id": "597f4fb2d8a0702df6b892eb"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5983b5ce03f95209c6bc5196",
        "passcode": "$2a$10$jMj6HbBNl9jqC.LtvRgsQ.CrgyRRL32kkZS2aVZAOFJe90mBtWE8y",
        "email": "test@test.com",
        "phone": "5701234567890",
        "lukatag": "@testing",
        "lastname": "test",
        "firstname": "test",
        "__v": 0,
        "created": "August 3rd 2017, 4:39:50 pm",
        "terms": [
            {
                "channel": "postman",
                "verified": true,
                "_id": "5983b5ce03f95209c6bc519b",
                "dateConfirmed": "August 3rd 2017, 4:39:50 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "postman",
                "name": "postman",
                "id": "postman",
                "status": "postman",
                "_id": "5983b5ce03f95209c6bc519a"
            }
        ],
        "location": [
            {
                "city": "Bogota",
                "country": "Colombia",
                "countryCode": "CO",
                "address": "st 1",
                "_id": "5983b5ce03f95209c6bc5199"
            }
        ],
        "identification": [
            {
                "numberId": "1234",
                "typeId": "cc",
                "dateIssue": "1223-12-12T05:00:00.000Z",
                "verified": true,
                "_id": "5983b5ce03f95209c6bc5197",
                "locationIssue": [
                    {
                        "city": "Bogota",
                        "country": "Colombia",
                        "countryCode": "CO",
                        "_id": "5983b5ce03f95209c6bc5198"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e10c0c5a75469e0008ad",
        "passcode": "$2a$10$JmfJO8leqthF7nX9ZLV5u.YPbhVJRUC7F4pPPNT4lKmS4Mbh1uHs2",
        "email": "oliver@minka.io",
        "phone": "5741234567",
        "lukatag": "@oliver",
        "lastname": "Oliver",
        "firstname": "Oliver",
        "__v": 0,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e10c0c5a75469e0008b2",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e10c0c5a75469e0008b1"
            },
            {
                "_id": "598a8b1191e8ff52456ecf47"
            },
            {
                "_id": "598a8d9c91e8ff52456ecf4b"
            },
            {
                "_id": "598a90bb91e8ff52456ecf4f"
            },
            {
                "_id": "598a932f91e8ff52456ecf53"
            },
            {
                "_id": "598a97a991e8ff52456ecf55"
            },
            {
                "_id": "598a99e591e8ff52456ecf56"
            },
            {
                "_id": "598a9b7591e8ff52456ecf57"
            },
            {
                "_id": "598b7d2091e8ff52456ecf5a"
            }
        ],
        "location": [
            {
                "_id": "5963e10c0c5a75469e0008b0",
                "address": "Address 1",
                "countryCode": "CO",
                "country": "Colombia",
                "city": "Bogota"
            }
        ],
        "identification": [
            {
                "_id": "5963e10c0c5a75469e0008ae",
                "verified": true,
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "typeId": "cc",
                "numberId": "512345678",
                "locationIssue": [
                    {
                        "_id": "5963e10c0c5a75469e0008af",
                        "countryCode": "CO",
                        "country": "Colombia",
                        "city": "Bogota"
                    }
                ]
            }
        ]
    },
    {
        "_id": "5963e05a0c5a75469e000891",
        "passcode": "$2a$10$lxGLKMNnwLRQMgzarn1e2eAB.vb/hD3/Qdi8Qh6hThkA0G5b3.PFG",
        "email": "tom@minka.io",
        "phone": "5701234567",
        "lukatag": "@tom",
        "lastname": "Biscan",
        "firstname": "Tomislav",
        "__v": 0,
        "state": null,
        "created": "July 10th 2017, 4:07:05 pm",
        "terms": [
            {
                "channel": "{{channel}}",
                "verified": true,
                "_id": "5963e05a0c5a75469e000896",
                "dateConfirmed": "July 10th 2017, 4:07:05 pm",
                "version": "v 1.0"
            }
        ],
        "channel": [
            {
                "type": "{{typeChannel}}",
                "name": "{{nameChannel}}",
                "id": "{{idChannel}}",
                "status": "{{statusChannel}}",
                "_id": "5963e05a0c5a75469e000895"
            },
            {
                "_id": "5976186e0a04272709e9b3ba",
                "metadata": {
                    "fbid": "98789789798798"
                },
                "status": "VERIFIED",
                "id": "123123123123123123",
                "name": "Hola Luka FB Dev Page",
                "type": "facebook"
            },
            {
                "_id": "597618a90a04272709e9b3bb",
                "metadata": {
                    "fbid": "1657992740886034"
                },
                "status": "VERIFIED",
                "id": "1980090698938811",
                "name": "facebook",
                "type": "facebook"
            },
            {
                "metadata": {
                    "fbid": "44444444444444444444"
                },
                "status": "VERIFIED",
                "id": "3333333333333333333",
                "name": "Hola Luka FB Dev Page",
                "type": "facebook",
                "_id": "597618a90a04272709e9b3bb"
            },
            {
                "metadata": {
                    "fbid": "55555555555555"
                },
                "_id": "597618a90a04272709e9b3bb"
            },
            {
                "_id": "5977d4d9d8a0702df6b891f1",
                "status": "123",
                "id": "123",
                "name": "123",
                "type": "123"
            }
        ],
        "location": [
            {
                "_id": "5963e05a0c5a75469e000894",
                "state": "Bogota district",
                "address": "Calle 93 # 14-71",
                "countryCode": "CO",
                "country": "Colombia",
                "city": "Bogota"
            }
        ],
        "identification": [
            {
                "_id": "5963e05a0c5a75469e000892",
                "verified": true,
                "dateIssue": "2007-01-31T00:00:00.000Z",
                "typeId": "cc",
                "numberId": "112345678",
                "locationIssue": [
                    {
                        "_id": "5963e05a0c5a75469e000893",
                        "countryCode": "CO",
                        "country": "Colombia",
                        "city": "Bogota"
                    }
                ]
            }
        ]
    }
]`
//console.log(profile.phone);
profile = JSON.parse(profile);
console.log(profile[1]._id);
//var result = JSON.parse(JSON.stringify(_.filter(person,function(row){return (true)})));
profile = _.filter(_.sortBy(profile,'_id'),function(row){return row.identification._id == `5963e05a0c5a75469e000893`});
console.log(profile.length);
/*
//var result = false;
for (var i=0;i<profile.channel.length;i++) {
	if(profile.channel[i].type == "facebook" && profile.channel[i].id == "123123123123123123")
		result = true;

}
console.log(result);
var M=require('../src/minka_api.js')
var m=new M();
m.access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5NjNlMDVhMGM1YTc1NDY5ZTAwMDg5MSIsImlhdCI6MTUwMDIxMzMxMn0.lyB-N4y-lHs1yx6G3Typn9z7zaunLblZameKzqhBPY0'
m.getPerson('tom', function(profile){
if (!profile) {
  console.log(`ERR: Cannot retrieve ${me.parameters.lukatag}`);
  
}
else {
  console.log(profile.channel.length)
}
});
*/