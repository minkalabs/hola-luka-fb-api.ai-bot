//Minka adapter for payment gateways
//implements 4 endpoints
// /gateway/in - cash in
// /gateway/out - cash out
// /gateway/verify - verify bill
// /gateway/pay - pay bill

'use strict';
var request=require('request');
var S = require('string');
const _ = require('underscore')
const i18n = require('i18n');
var session_id = '';

class adapter_sintesis {

  constructor () {

    //Minka API reference
    this.gateway = undefined;
    //sintesis session id

  }

  //initialize Sintesis session
  //session exists 30 minutes and has to be recheked at every call
  initializeSession(cb) {
    //post initsession to this.gateway.parameters.initializeSessionURL

console.log(JSON.stringify({
      method: 'POST',
      url: `${this.gateway.parameters.initializeSessionURL}`,
      body: {
        usuario: `${this.gateway.parameters.username}`,
        password: `${this.gateway.parameters.password}`,
        origenTransaccion: `${this.gateway.parameters.transactionSource}`
        }
    }));

    request({
      method: 'POST',
      url: `${this.gateway.parameters.initializeSessionURL}`,
      body: {
        usuario: `${this.gateway.parameters.username}`,
      	password: `${this.gateway.parameters.password}`,
      	origenTransaccion: `${this.gateway.parameters.transactionSource}`
        },
      json: true
    }, function (error, response, body) {
      //create new session
      console.log(`Sintesis session ${JSON.stringify(body)}`)
      if (response.statusCode === 200) {
        session_id = body.idOperativo;
        console.log(`New session ${session_id}`);
        cb(true)
      }
      else {
        console.log(`Error initializing Sintesis session.`);
        cb(false)
      }
    });

  }

  verifySession(cb) {
    //if seesion_id empty initialize it
    console.log(`verifiying sintesis session`)
    var recall = 0;
    var me = this;
    if (session_id == '') {
      console.log(`it is empty, let's get new one`)
      this.initializeSession(function(success){
        cb(success);
      })
    }
    //if session_id not empty check if it is active
    else {
      console.log(`it is not empty, but let's see if it is valid`);
      request(`${this.gateway.parameters.verifySessionURL}/${session_id}`, function (error, response, body) {
        //check status code
        //FIX
        if (response == undefined) {
          console.log(`DANGER ZONE, FIX ${session_id}`);
          cb(true);
        }
        if (response.statusCode === 200) {
          //session is still valid
          console.log(`Old session ${session_id}`);
          //FIX: cb(true);
          me.initializeSession(function(success){
            cb(success);
          })

        }
        else {
          //session is invalid, initialize it again
          console.log(`it is invalid, let's re-initialize it`);
          me.initializeSession(function(success){
            cb(success);
          })
        }
      });
    }
  }

  //cash-in method
  in(id) {

  }

  //cash-out method
  out(id) {

  }

  //verify bill method
  verifyBill(bill,cb) {
    //check session_id and re-initialize if expired
    var gateway = this;
    console.log(`verifing bill ${JSON.stringify(bill)}`);
    console.log(`first get Sintesis session`);
    this.verifySession(function(success){
      if (success) {
        console.log(`verifySession success`);
        console.log(`we need pending bills for ${bill.metadata.accountId}, ${bill.metadata.internalStoreId}, ${bill.metadata.internalServiceId}`);
        //get a list of pending bills for transactionId and date
        gateway.pendingBills(bill.metadata.accountId, bill.metadata.internalStoreId, bill.metadata.internalServiceId, function(items) {
          if (items !== false) {
            console.log(`pendingBills success`);
            if (bill.metadata.name.indexOf('Yanbal')>-1) {
              var s = items.pendingBills[0].item[0].description;
              s = s.substring(s.indexOf('(')+1,s.indexOf(')')).replace('    ','')
              s = s.substring(s.indexOf('=')+1,s.indexOf('$')).trim();
              s = parseFloat(s*6.96).toFixed(2);
              s = `${s} Bs`;
              s = `Saldo referencial ${s}`
              bill.metadata.description = `Nombre ${items.pendingBills[0].account.name}, ${s}`;
            }
            bill.status = "PENDING";
            cb(bill);
          }
          else {
            //bill.metadata.error = `Bill doesn't exist`;
            bill.status = "INVALID";
            bill.metadata.updated = new Date().toISOString();
            //console.log(`payment_gateway_sintesis ${bill}`)
            cb(bill);
          }
        });
      }
      else {
        bill.metadata.error = `Inavlid Sintesis session`;
        bill.status = "DRAFT";
        bill.metadata.updated = new Date().toISOString();
        //console.log(`payment_gateway_sintesis ${bill}`)
        cb(bill);
      }
    });

  }

  //getPaymentURL method
  getPaymentURL(payer, bill, cb) {
    //check session_id and re-initialize if expired
    var gateway = this;
    var paymentURL = "";
    var items = undefined;
    console.log(`getPaymentURL function`);
    this.verifySession(function(success){
      if (success) {
        console.log(`verifySession success`);
        //get a list of pending bills for transactionId and date
        gateway.pendingBills(bill.metadata.accountId, bill.metadata.internalStoreId, bill.metadata.internalServiceId, function(items) {
          if (items !== false) {
            console.log(`pendingBills for processGetPaymentURL success`);

            //pay bill through Sintesis
            gateway.processGetPaymentURL(payer, items, bill, function(success){

              if (success) {
                console.log(`processGetPaymentURL success`);
                // this.gateway.parameters.paymentURL = "https://test.sintesis.com.bo/luka/#/pay?entidad=[ENTIDAD]&ref=[REF]%2F20&red=[RED]";
                paymentURL = gateway.gateway.parameters.paymentURL;
                paymentURL = paymentURL.replace('[ENTIDAD]', bill.metadata.internalStoreId);
                //"idOperativo": "${session_id}",
                //"nroOperacion": "${items.transactionId}",
                paymentURL = paymentURL.replace('[REF]', `${session_id}/${items.transactionId}`);
                console.log(`paymentURL = ${paymentURL}`);

                cb(paymentURL);
              }
              else {
                console.log(`processGetPaymentURL error`);
                bill.metadata.error = `Error creating payment_url`;
                bill.metadata.updated = new Date().toISOString();
                cb(bill);
              }
            });

          }
          else {
            bill.metadata.error = `Bill doesn't exist`;
            bill.status = "INVALID";
            bill.metadata.updated = new Date().toISOString();
            //console.log(`payment_gateway_sintesis ${bill}`)
            cb(bill);
          }
        });
      }
      else {
        bill.status = "PENDING";
        bill.metadata.updated = new Date().toISOString();
        //console.log(`payment_gateway_sintesis ${bill}`)
        cb(bill);
      }
    });

  }


  processGetPaymentURL(payer, items, bill, cb){

    var phone = "";
    console.log(`Finding a phone..`);
    for (var c=0;c<payer.contacts.length;c++) {
      if (payer.contacts[c].type=="phone") {
        phone = payer.contacts[c].value;
      }
    }
    console.log(`phone is ${phone}`);

    var email = "";
    console.log(`Finding an email..`);
    for (var c=0;c<payer.contacts.length;c++) {
      if (payer.contacts[c].type=="email") {
        email = payer.contacts[c].value;
      }
    }
    console.log(`email is ${email}`);


    console.log(`processGetPaymentURL wait`);
    console.log(`{"registraPagoRequest": {
      "idOperativo": "${session_id}",
      "nroOperacion": "${items.transactionId}",
      "fechaOperativa": "${items.transactionDate}",
      "codModulo": ${bill.metadata.internalStoreId},
      "cuenta": "${bill.metadata.accountId}",
      "servicio": ${items.pendingBills[0].account.serviceId},
      "nombreFac": "${payer.name[0].first} ${payer.name[0].first}",
      "items": [
          {
              "nroItem": "${items.pendingBills[0].item[0].number}",
              "monto": ${parseFloat(bill.value).toFixed(1)}
          }
      ]
    },
    "tarjetaHabiente":
     {
       "firstName":"${payer.name[0].first}",
       "lastName":"${payer.name[0].last}",
       "email":"${email}",
       "phone":"${phone}",
       "country":"${payer.locations[0].country}",
       "state":"${payer.locations[0].state}",
       "city":"${payer.locations[0].city}",
       "address":"${payer.locations[0].street}"
     }}`);

    request({
      method: 'POST',
      url: `${this.gateway.parameters.verifyPaymentURL}`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{"registraPagoRequest": {
        "idOperativo": "${session_id}",
        "nroOperacion": "${items.transactionId}",
        "fechaOperativa": "${items.transactionDate}",
        "codModulo": ${bill.metadata.internalStoreId},
        "cuenta": "${bill.metadata.accountId}",
        "servicio": ${items.pendingBills[0].account.serviceId},
        "nombreFac": "${payer.name[0].first} ${payer.name[0].last}",
        "items": [
            {
                "nroItem": "${items.pendingBills[0].item[0].number}",
                "monto": ${parseFloat(bill.value).toFixed(1)}
            }
        ]
      },
      "tarjetaHabiente":
       {
         "firstName":"${payer.name[0].first}",
         "lastName":"${payer.name[0].last}",
         "email":"${email}",
         "phone":"${phone}",
         "country":"${payer.locations[0].country}",
         "state":"${payer.locations[0].country}",
         "city":"${payer.locations[0].city}",
         "address":"${payer.locations[0].street}"
       }}`
    }, function (error, response, body) {
      //verify if ATC Payment is valid
      console.log(`ATC response: ${JSON.stringify(response)} ${JSON.stringify(body)} ${body.code} ${JSON.stringify(error)}`);
      body = JSON.parse(body);

      if (body.code == 0) {
        console.log(`ATC payment verified using Sintesis, session_id = ${session_id}`);
        cb(true)
      }
      else {
        console.log(`Error initializing Sintesis session. Error ${body.codError}, ${body.mensaje}`);
        cb(false)
      }
    });

  }


  //pay method
  payBill(bill, cb) {

    //if bill is alread paid just return it
    if (bill.status == "PAID") {
      return cb(bill);
      }

    //check session_id and re-initialize if expired
    var gateway = this;
    this.verifySession(function(success){
      if (success) {
        console.log(`verifySession success`);
        //get a list of pending bills for transactionId and date
        gateway.pendingBills(bill.metadata.accountId, bill.metadata.internalStoreId, bill.metadata.internalServiceId, function(items) {
          if (items !== false) {
            console.log(`pendingBills success`);

            //pay bill through Sintesis
            gateway.processPayBill(items, bill, function(success){

              if (success) {
                console.log(`processPayBill success`);
                bill.status = "PAID";
                bill.metadata.updated = new Date().toISOString();
                cb(bill);
              }
              else {
                console.log(`processPayBill error`);
                bill.metadata.error = `Error processing bill`;
                bill.status = "PENDING";
                bill.metadata.updated = new Date().toISOString();
                cb(bill);
              }
            });

          }
          else {
            bill.metadata.error = `Bill doesn't exist`;
            bill.status = "INVALID";
            bill.metadata.updated = new Date().toISOString();
            //console.log(`payment_gateway_sintesis ${bill}`)
            cb(bill);
          }
        });
      }
      else {
        bill.status = "PENDING";
        bill.metadata.updated = new Date().toISOString();
        //console.log(`payment_gateway_sintesis ${bill}`)
        cb(bill);
      }
    });

  }


  processPayBill(items, bill, cb){
    console.log(`processPayBill wait`);
    console.log(`{
      "idOperativo": "${session_id}",
      "nroOperacion": "${items.transactionId}",
      "fechaOperativa": "${items.transactionDate}",
      "codModulo": ${bill.metadata.internalStoreId},
      "cuenta": "${bill.metadata.accountId}",
      "servicio": ${items.pendingBills[0].account.serviceId},

      "items": [
          {
              "nroItem": "${items.pendingBills[0].item[0].number}",
              "monto": ${parseFloat(bill.value).toFixed(1)}
          }
      ]
      }`);
    request({
      method: 'POST',
      url: `${this.gateway.parameters.payBillURL}`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{
        "idOperativo": "${session_id}",
        "nroOperacion": "${items.transactionId}",
        "fechaOperativa": "${items.transactionDate}",
        "codModulo": ${bill.metadata.internalStoreId},
        "cuenta": "${bill.metadata.accountId}",
        "servicio": ${items.pendingBills[0].account.serviceId},

        "items": [
            {
                "nroItem": "${items.pendingBills[0].item[0].number}",
                "monto": ${parseFloat(bill.value).toFixed(1)}
            }
        ]
        }`
    }, function (error, response, body) {
      //create new session
      if (response.statusCode === 200) {
        console.log(`Bill paid on Sintesis, session_id = ${session_id}`);
        cb(true)
      }
      else {
        console.log(`Error initializing Sintesis session. Error ${body.codError}, ${body.mensaje}`);
        cb(false)
      }
    });

  }

  //pay subscription method
  paySubscription(subscription, cb) {
    subscription.status = "PAID";
    console.log(`payment_gateway ${subscription}`)
    cb(subscription);

  }

  //lit of pending items/bills
  pendingBills(accountId, storeId, serviceId, cb){

    //special cases
    if (storeId == "7" && serviceId == "1001") {
      //case entel
      //codigo has a phone number without 591 and service number
      var phone = me.luka.profile.phone.substring(3, me.luka.profile.phone.length);
      accountId = `"${phone}","${accountId}"`;
    }


    console.log(`let's get pending bills {"idOperativo": "${session_id}","codModulo": "${storeId}","codCriterio": "${serviceId}","codigo": ["${accountId}"]}`);
    request({
      method: 'POST',
      url: `${this.gateway.parameters.getBillsURL}`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{
        "idOperativo": "${session_id}",
        "codModulo": "${storeId}",
        "codCriterio": "${serviceId}",
        "codigo": [
        	"${accountId}"
        ]
        }`
    }, function (error, response, body) {
      //create new session
      console.log(`pendingBills() ${error} ${JSON.stringify(response)} ${body}`);
      if (response.statusCode === 200) {
        cb(JSON.parse(body));
      }
      else {
        console.log(`Error calling pendingBills().`);
        cb(false)
      }
    });
  }
}

module.exports = adapter_sintesis;
