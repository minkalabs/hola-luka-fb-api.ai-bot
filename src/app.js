/*****/
// "Hola Luka" chat bot - Facebook via API.AI via Minka API
// Author: Tomislav Bišćan, tomi@minka.io
/*****/
'use strict';

const apiai = require('./apiai/index.js');
const express = require('express');
const bodyParser = require('body-parser');
const uuid = require('node-uuid');
const request = require('request');
const JSONbig = require('json-bigint');
const cJSON = require('circular-json')
//const JSON = require('circular-json')
const async = require('async');
var rp = require('request-promise');
var dateFormat = require('dateformat');

const config = require('../app.json')

const REST_PORT = (config.env.PORT || 5000);
const APIAI_ACCESS_TOKEN = config.env.APIAI_ACCESS_TOKEN.value;
const APIAI_LANG = config.env.APIAI_LANG.value || 'en';
const FB_VERIFY_TOKEN = config.env.FB_VERIFY_TOKEN.value;
const FB_PAGE_ACCESS_TOKEN = config.env.FB_PAGE_ACCESS_TOKEN.value;
const FB_PAGE_URL = config.env.FB_PAGE_URL.value;
const FB_TEXT_LIMIT = 640;
const MINKA_API_URL = config.env.MINKA_API_URL.value;
const BOT_APP_URL = config.env.BOT_APP_URL.value;
const EMAIL_RECEIPT_TEMPLATE_NUMBER = config.env.EMAIL_RECEIPT_TEMPLATE_NUMBER.value;

const Luka = require('./minka_api.js');
var luka = new Luka();
var sessions = {};
var sessionId, session;

class FacebookBot {
    constructor() {
        this.apiAiService = apiai(APIAI_ACCESS_TOKEN, {language: APIAI_LANG, requestSource: "fb"});
        this.sessionIds = new Map();
        this.messagesDelay = 2000;
    }


    doDataResponse(sender, facebookResponseData) {
        if (!Array.isArray(facebookResponseData)) {
            console.log('Response as formatted message');
            this.sendFBMessage(sender, facebookResponseData)
                .catch(err => console.error(err));
        } else {
            async.eachSeries(facebookResponseData, (facebookMessage, callback) => {
                if (facebookMessage.sender_action) {
                    console.log('Response as sender action');
                    this.sendFBSenderAction(sender, facebookMessage.sender_action)
                        .then(() => callback())
                        .catch(err => callback(err));
                }
                else {
                    console.log('Response as formatted message');
                    this.sendFBMessage(sender, facebookMessage)
                        .then(() => callback())
                        .catch(err => callback(err));
                }
            }, (err) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log('Data response completed');
                }
            });
        }
    }

    doRichContentResponse(sender, messages) {
        let facebookMessages = []; // array with result messages

        for (let messageIndex = 0; messageIndex < messages.length; messageIndex++) {
            let message = messages[messageIndex];

            switch (message.type) {
                //message.type 0 means text message
                case 0:
                    // speech: ["hi"]
                    // we have to get value from fulfillment.speech, because of here is raw speech
                    if (message.speech) {

                        let splittedText = this.splitResponse(message.speech);

                        splittedText.forEach(s => {
                            facebookMessages.push({text: s});
                        });
                    }

                    break;
                //message.type 1 means card message
                case 1: {
                    let carousel = [message];

                    for (messageIndex++; messageIndex < messages.length; messageIndex++) {
                        if (messages[messageIndex].type == 1) {
                            carousel.push(messages[messageIndex]);
                        } else {
                            messageIndex--;
                            break;
                        }
                    }

                    let facebookMessage = {};
                    carousel.forEach((c) => {
                        // buttons: [ {text: "hi", postback: "postback"} ], imageUrl: "", title: "", subtitle: ""

                        let card = {};

                        card.title = c.title;
                        card.image_url = c.imageUrl;
                        if (this.isDefined(c.subtitle)) {
                            card.subtitle = c.subtitle;
                        }
                        //If button is involved in.
                        if (c.buttons.length > 0) {
                            let buttons = [];
                            for (let buttonIndex = 0; buttonIndex < c.buttons.length; buttonIndex++) {
                                let button = c.buttons[buttonIndex];

                                if (button.text) {
                                    let postback = button.postback;
                                    if (!postback) {
                                        postback = button.text;
                                    }

                                    let buttonDescription = {
                                        title: button.text
                                    };

                                    if (postback.startsWith("http")) {
                                        buttonDescription.type = "web_url";
                                        buttonDescription.url = postback;
                                    } else {
                                        buttonDescription.type = "postback";
                                        buttonDescription.payload = postback;
                                    }

                                    buttons.push(buttonDescription);
                                }
                            }

                            if (buttons.length > 0) {
                                card.buttons = buttons;
                            }
                        }

                        if (!facebookMessage.attachment) {
                            facebookMessage.attachment = {type: "template"};
                        }

                        if (!facebookMessage.attachment.payload) {
                            facebookMessage.attachment.payload = {template_type: "generic", elements: []};
                        }

                        facebookMessage.attachment.payload.elements.push(card);
                    });

                    facebookMessages.push(facebookMessage);
                }

                    break;
                //message.type 2 means quick replies message
                case 2: {
                    if (message.replies && message.replies.length > 0) {
                        let facebookMessage = {};

                        facebookMessage.text = message.title ? message.title : 'Choose an item';
                        facebookMessage.quick_replies = [];

                        message.replies.forEach((r) => {
                            facebookMessage.quick_replies.push({
                                content_type: "text",
                                title: r,
                                payload: r
                            });
                        });

                        facebookMessages.push(facebookMessage);
                    }
                }

                    break;
                //message.type 3 means image message
                case 3:

                    if (message.imageUrl) {
                        let facebookMessage = {};

                        // "imageUrl": "http://example.com/image.jpg"
                        facebookMessage.attachment = {type: "image"};
                        facebookMessage.attachment.payload = {url: message.imageUrl};

                        facebookMessages.push(facebookMessage);
                    }

                    break;
                //message.type 4 means custom payload message
                case 4:
                    if (message.payload && message.payload.facebook) {
                        facebookMessages.push(message.payload.facebook);
                    }
                    break;

                default:
                    break;
            }
        }

        return new Promise((resolve, reject) => {
            async.eachSeries(facebookMessages, (msg, callback) => {
                    this.sendFBSenderAction(sender, "typing_on")
                        .then(() => this.sleep(this.messagesDelay))
                        .then(() => this.sendFBMessage(sender, msg))
                        .then(() => callback())
                        .catch(callback);
                },
                (err) => {
                    if (err) {
                        console.error(err);
                        reject(err);
                    } else {
                        console.log('Messages sent');
                        resolve();
                    }
                });
        });

    }

    doTextResponse(sender, responseText) {
        console.log('Response as text message');
        // facebook API limit for text length is 640,
        // so we must split message if needed
        let splittedText = this.splitResponse(responseText);

        async.eachSeries(splittedText, (textPart, callback) => {
            this.sendFBMessage(sender, {text: textPart})
                .then(() => callback())
                .catch(err => callback(err));
        });
    }
    //which webhook event
    getEventText(event) {
        if (event.message) {
            if (event.message.quick_reply && event.message.quick_reply.payload) {
                return event.message.quick_reply.payload;
            }

            if (event.message.text) {
                return event.message.text;
            }
        }

        if (event.postback && event.postback.payload) {
            return event.postback.payload;
        }

        return null;

    }

    processEvent(event) {

        console.log("Event ", JSON.stringify(event));

        const sender = event.sender.id.toString();
        const receiver = event.recipient.id.toString();
        const text = this.getEventText(event);

        var me = this;

        //try to get the person for this senderid
        luka.getPerson(sender, function(person){
        //luka.getPerson("tom", function(person){

        if (text) {

            // Handle a text message from this sender
            if (!me.sessionIds.has(sender)) {
                me.sessionIds.set(sender, uuid.v4());
                console.log(`new session ${sender}`);
            }
            sessionId = me.sessionIds.get(sender);
            //handle bot session for every user
            //findOrCreateSession
            if (sessions[sessionId] == undefined) {
                console.log(`New session`);
                sessions[sessionId] = {sessionId: sessionId, fbid: sender, fbpageid: receiver, is_admin: true, context: {}};
                sessions[sessionId].context.luka = new Luka();
                sessions[sessionId].context.luka.sender_id = sender;
                sessions[sessionId].context.luka.bot = me;
            }
            console.log(`SessionId ${sender} ${me.sessionIds.get(sender)}`);

            //if a person has a channel set it as authenticated, otherwise wipe it
            if (person) {
               sessions[sessionId].context.luka.profile = person;
               sessions[sessionId].context.luka.authenticated = true;
               console.log(`You have a channel ${JSON.stringify(sessions[sessionId].context.luka.profile.lukatag)} ${JSON.stringify(person)}`);
            }
            else {
               sessions[sessionId].context.luka.profile = undefined;
               sessions[sessionId].context.luka.authenticated = false;
               console.log(`You don't have a channel`);
            }

            //send user's text to api.ai service
            let apiaiRequest = me.apiAiService.textRequest(text,
                {
                    sessionId: me.sessionIds.get(sender),
                    originalRequest: {
                        data: event,
                        source: "facebook"
                    }
                });
            //get response from api.ai
            apiaiRequest.on('response', (response) => {
                if (me.isDefined(response.result) && me.isDefined(response.result.fulfillment)) {
                    let responseText = response.result.fulfillment.speech;
                    let responseData = response.result.fulfillment.data;
                    let responseMessages = response.result.fulfillment.messages;

                    let action = response.result.action;

                    if (me.isDefined(responseData) && me.isDefined(responseData.facebook)) {
                        let facebookResponseData = responseData.facebook;
                        me.doDataResponse(sender, facebookResponseData);
                    } else if (me.isDefined(responseMessages) && responseMessages.length > 0) {
                        me.doRichContentResponse(sender, responseMessages);
                    }
                    else if (me.isDefined(responseText)) {
                        me.doTextResponse(sender, responseText);
                    }

                }
            });

            apiaiRequest.on('error', (error) => console.error(error));
            apiaiRequest.end();
        }
        })
    }

    splitResponse(str) {
        if (str.length <= FB_TEXT_LIMIT) {
            return [str];
        }

        return this.chunkString(str, FB_TEXT_LIMIT);
    }

    chunkString(s, len) {
        let curr = len, prev = 0;

        let output = [];

        while (s[curr]) {
            if (s[curr++] == ' ') {
                output.push(s.substring(prev, curr));
                prev = curr;
                curr += len;
            }
            else {
                let currReverse = curr;
                do {
                    if (s.substring(currReverse - 1, currReverse) == ' ') {
                        output.push(s.substring(prev, currReverse));
                        prev = currReverse;
                        curr = currReverse + len;
                        break;
                    }
                    currReverse--;
                } while (currReverse > prev)
            }
        }
        output.push(s.substr(prev));
        return output;
    }

    sendFBMessage(sender, messageData) {
        console.log(`sendFBMessage {
                url: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {access_token: ${FB_PAGE_ACCESS_TOKEN},
                method: 'POST',
                json: {
                    recipient: {id: ${sender},
                    message: ${JSON.stringify(messageData)}
                }
            }`);
        return new Promise((resolve, reject) => {
            request({
                url: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {access_token: FB_PAGE_ACCESS_TOKEN},
                method: 'POST',
                json: {
                    recipient: {id: sender},
                    message: messageData
                }
            }, (error, response) => {
                if (error) {
                    console.log('Error sending message: ', error);
                    reject(error);
                } else if (response.body.error) {
                    console.log('Error: ', response.body.error);
                    reject(new Error(response.body.error));
                }

                resolve();
            });
        });
    }

    sendFBSenderAction(sender, action) {
        console.log(`sendFBSenderAction {
                url: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {access_token: ${FB_PAGE_ACCESS_TOKEN},
                method: 'POST',
                json: {
                    recipient: {id: ${sender},
                    sender_action: ${action}
                }
            }`);
        return new Promise((resolve, reject) => {
            request({
                url: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {access_token: FB_PAGE_ACCESS_TOKEN},
                method: 'POST',
                json: {
                    recipient: {id: sender},
                    sender_action: action
                }
            }, (error, response) => {
                if (error) {
                    console.error('Error sending action: ', error);
                    reject(error);
                } else if (response.body.error) {
                    console.error('Error: ', response.body.error);
                    reject(new Error(response.body.error));
                }

                resolve();
            });
        });
    }

  addRequestEvents(event, options, session){

    var me = this;
    var sender = session.fbid;
    var _apiai = new apiai(APIAI_ACCESS_TOKEN, {language: APIAI_LANG})
    var apiaiRequestEvent = _apiai.eventRequest(event,options);

    apiaiRequestEvent.on('response', function(response) {

        if (me.isDefined(response.result) && me.isDefined(response.result.fulfillment)) {
            let responseText = response.result.fulfillment.speech;
            let responseData = response.result.fulfillment.data;
            let responseMessages = response.result.fulfillment.messages;

            let action = response.result.action;

            if (me.isDefined(responseData) && me.isDefined(responseData.facebook)) {
                let facebookResponseData = responseData.facebook;
                me.doDataResponse(sender, facebookResponseData);
            } else if (me.isDefined(responseMessages) && responseMessages.length > 0) {
                me.doRichContentResponse(sender, responseMessages);
            }
            else if (me.isDefined(responseText)) {
                me.doTextResponse(sender, responseText);
            }

        }


/*
      for (var nResponses = 0; nResponses < response.result.fulfillment.messages.length; nResponses++) {
        var apiResponseText = response.result.fulfillment.messages[nResponses].speech;
        if (apiResponseText != '' && apiResponseText != undefined) me.sendFBMessage(session.fbid, {text: apiResponseText});
      }
*/
    });

    apiaiRequestEvent.on('error', function(error) {
        console.log(error);
    });

    apiaiRequestEvent.end();
  }

    doSubscribeRequest() {
        request({
                method: 'POST',
                uri: "https://graph.facebook.com/v2.6/me/subscribed_apps?access_token=" + FB_PAGE_ACCESS_TOKEN
            },
            (error, response, body) => {
                if (error) {
                    console.error('Error while subscription: ', error);
                } else {
                    console.log('Subscription result: ', response.body);
                }
            });
    }

    isDefined(obj) {
        if (typeof obj == 'undefined') {
            return false;
        }

        if (!obj) {
            return false;
        }

        return obj != null;
    }

    sleep(delay) {
        return new Promise((resolve, reject) => {
            setTimeout(() => resolve(), delay);
        });
    }

}


let facebookBot = new FacebookBot();

const app = express();
app.use(express.static('public'));
app.use(bodyParser.text({type: 'application/json'}));
app.set('view engine', 'ejs');

app.get('/webhook', (req, res) => {
    if (req.query['hub.verify_token'] == FB_VERIFY_TOKEN) {
        res.send(req.query['hub.challenge']);

        setTimeout(() => {
            facebookBot.doSubscribeRequest();
        }, 3000);
    } else {
        res.send('Error, wrong validation token');
    }
});

app.post('/webhook', (req, res) => {
    try {
        const data = JSONbig.parse(req.body);

        if (data.entry) {
            let entries = data.entry;
            entries.forEach((entry) => {
                let messaging_events = entry.messaging;
                if (messaging_events) {
                    messaging_events.forEach((event) => {
                        if (event.message && !event.message.is_echo ||
                            event.postback && event.postback.payload) {
                            facebookBot.processEvent(event);
                        }
                    });
                }
            });
        }

        return res.status(200).json({
            status: "ok"
        });
    } catch (err) {
        return res.status(400).json({
            status: "error",
            error: err
        });
    }

});

//api.ai webhook
app.post('/apiai', (req, res) => {

    //API.AI object
    const action = JSON.parse(req.body).result.action;
    const actionIncomplete = JSON.parse(req.body).result.actionIncomplete;
    var parameters = JSON.parse(req.body).result.parameters;
    var contexts = JSON.parse(req.body).result.contexts;
    const intentName = JSON.parse(req.body).result.intentName;
    var sessionId = JSON.parse(req.body).sessionId;
    var source = ''; //Facebook, Kik, Telegram...

    //find source (Facebook, Kik, Telegram...)
    switch (facebookBot.apiAiService.requestSource) {
        case "fb":
            source = "facebook";
        break;
    }

    //get session
    session = sessions[sessionId];
    //console.log(`webhook session ${JSON.stringify(session)}`)
    //console.log(`contexts ${JSON.stringify(contexts)}`)
    //console.log(`parameters ${JSON.stringify(parameters)}`)

    //initiate action object
    const ActionObject = require(`./actions/${action}.js`);
    const actionObject = new ActionObject(actionIncomplete, parameters, contexts, intentName, session, source, facebookBot);

    //execute default action and forward response
    const result = actionObject.execute(function(response){
      console.log(`webhook return: ${JSON.stringify(response)}`);
      return res.json(response);
    })


});


app.post('/gateway/pay', (req, res) => {
  console.log(`new pay request`)

  var bill = req.body;

  var Adapter = require('./gateway_adapter.js');
  var adapter = new Adapter();

  console.log(`req.body.bill ${bill}`)
  adapter.pay(bill, function(processed_bill) {
      console.log(`/gateway/pay/bill ${JSON.stringify(processed_bill)}`)
      res.end(`${JSON.stringify(processed_bill)}`)
  });

})

app.post('/gateway/verify', (req, res) => {
  console.log(`new verify request`)
  var Adapter = require('./gateway_adapter.js');
  var adapter = new Adapter();
  //var bill = JSON.stringify(req.body);
  var bill = req.body;
  console.log(`req.body.bill ${bill}`)
  adapter.verify(bill, function(processed_bill) {
      console.log(`/gateway/verify/bill ${JSON.stringify(processed_bill)}`)
      res.end(`${JSON.stringify(processed_bill)}`)
  });

})

app.post('/gateway/sintesis/cc_debit/confirm', (req, res) => {

  //process pay bill

  // {	"atcPayment": 0 ,
  // 	"servicePayment": 0,
  // 	"ref": "transactionID",
  // 	"txId": "ROUTED_PARAMETER"
  // }

  console.log(`ATC confirm payment received: ${JSON.parse(req.body).txId}`)

  var billId = JSON.parse(req.body).txId;
  var sessId = JSON.parse(req.body).sessionId;
  var retObject = undefined;
  var paymentMethod = 'ATC';
  var bill = undefined;
  var service = undefined;
  var store = undefined;
  var event = undefined;
  var options = undefined;
  var sequence = Promise.resolve();

  var me = this;
  session = sessions[sessId];

  if (billId == undefined || session == undefined || JSON.parse(req.body).atcPayment != 0) {
    retObject = {
        error: 1,
        message: "txId undefined, session doesn't exist or atcPayment error."
    }

  console.log(`PAYMENT REJECTED`);

  if (session != undefined) {
    options = {sessionId: `${session.sessionId}`};
    event = {name: `bill-pay-fail`,data: {bill: `${billId}`, paymentMethod: paymentMethod}};
    facebookBot.addRequestEvents(event,options,session);
  }


  res.setHeader('content-type', 'application/json');
  res.end(`${JSON.stringify(retObject)}`)

  return;

  }

  console.log(`Processing payment for billId ${billId} txId ${JSON.parse(req.body).atcPayment}...`);
  console.log(`Session is ${cJSON.stringify(session)} for ${sessId}, fbid is ${session.fbid}`)

  sequence = sequence.then(function(){
    //try to get a bill
    return new Promise(function(resolve, reject) {
        luka.getBill(billId, function(_bill){
          //check result
          if (!_bill) {
            console.log(`ERR: Cannot retrieve bill for ${billId}`);
            return reject('error')
          }
          else
            console.log(`We have a bill ${billId}...`);
            bill = _bill;
            return resolve();
        });
    });
  })
  .then(function() {
  //get service details
    //try to get services
    console.log(`getting the service`)
    return new Promise(function(resolve, reject) {
        luka.getService(bill.metadata.service, function(_service){
          //check result
          if (!_service) {
            console.log(`ERR: Cannot retrieve services for filter ${bill.metadata.service}`);
            return reject('error')
          }
          else
            console.log(`we have it here ${JSON.stringify(_service)}`)
            service = _service;
            return resolve();
        });
    });

  })
  .then(function() {
  //get store details
    //try to get store
    console.log(`getting the store for ${service.org}`)
    return new Promise(function(resolve, reject) {
        luka.getStore(bill.metadata.store, function(_store){
          //check result
          if (!_store) {
            console.log(`ERR: Cannot retrieve store for filter ${bill.metadata.store}`);
            return reject('error')
          }
          else
            console.log(`we have it here ${JSON.stringify(_store)}`)
            store = _store;
            return resolve();
        });
    });
  })
  .then(function(){
    return new Promise(function(resolve, reject) {
      //send payment: issuer -> user
      luka.sendPayment(`@${bill.currency.toLowerCase()}`, bill.source, bill.value, bill.currency, `Initial TOPUP from ${paymentMethod}`, function(txid){
        //check result
        if (!txid) {
          console.log(`ERR: Cannot make payment to ${bill.source}`);
          return reject()
        }
        else {
          console.log(`Transaction created ${txid}...`);
          return resolve(txid);
        }
      });
    })
  })
  .then(function(txid){
    return new Promise(function(resolve, reject) {
      //send payment: issuer -> user
      luka.verifyTransaction(txid, function(result){
        //check result
        if (!result) {
          console.log(`ERR: Cannot verify payment to ${bill.source}`);
          return reject()
        }
        else {
          console.log(`Transaction verified ${result}...`);
          return resolve();
        }
      });
    })
  })
  .then(function() {
  //pay the bill; just mark it paid

    return new Promise(function(resolve, reject) {

      bill.metadata.paymentMethod = "ATC";
      var options = {
          method: 'PUT',
          uri: `${MINKA_API_URL}/transfer/${billId}`,
          body: {data:{status:"PAID"}},
          json: true // Automatically stringifies the body to JSON
      };

      rp(options)
        .then(function(_bill){
          console.log(`paying bill ${JSON.stringify(_bill)}`)
          return resolve();
        });


    });
  })
  .then(function() {
  //save bill
    return new Promise(function(resolve, reject) {
      console.log(`verifying payBill ${bill.metadata.billId}... /transfer/${bill._id}/authorize`)
      luka.verifyTransaction(bill._id, function(success){
        if (!success) {
          options = {sessionId: `${session.sessionId}`};
          event = {name: `error`,data: {}};
          facebookBot.addRequestEvents(event,options,session);
        }
        else {

          console.log("Displaying final success");
          event = undefined;
          options = undefined;

          options = {sessionId: `${session.sessionId}`};
          //if a bill has a service define finish it as Service.Pay.Success and offer to subscribe, if it has a subscription do Subscription.Pay.Success don't offer subscribe, otherwise finish as Bil.Pay.Success
          if (bill.metadata.accountId == undefined && bill.metadata.service == undefined) {
            event = {name: `bill-pay-success`,data: {bill: `${bill.metadata.billId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`}};
            facebookBot.addRequestEvents(event,options,session);
          }
          else if (bill.metadata.accountId == '' || bill.metadata.service == '') {
            event = {name: `bill-pay-success`,data: {bill: `${bill.metadata.billId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`}};
            facebookBot.addRequestEvents(event,options,session);
          }
          else if (bill.metadata.accountId != '' && bill.metadata.service != '' && bill.metadata.subscription == '') {
            event = {name: `service-pay-success`,data: {bill: `${bill.metadata.accountId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`, account: `${bill.metadata.accountId}`, service: `${bill.metadata.service}`}};
            facebookBot.addRequestEvents(event,options,session);
          }
          else if (bill.metadata.accountId != '' && bill.metadata.service != '' && bill.metadata.subscription != '') {
            event = {name: `subscription-pay-success`,data: {bill: `${bill.metadata.accountId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`, account: `${bill.metadata.accountId}`, subscription: `${bill.metadata.subscription}`}};
            facebookBot.addRequestEvents(event,options,session);
          }
          return resolve();
        }
      })
    });
  })
  .then(function(){
    //try to retrieve person
    return new Promise(function(resolve, reject) {

      luka.getPerson(session.fbid, function(profile){
        if (profile.lukatag == undefined) {
          console.log(`ERR: Cannot retrieve ${parameters.lukatag}`);
          return reject('error')
        }
        else {
          console.log('got person')
            luka.profile = profile;
            return resolve();
          }
      })
    });
  })
  .then(function() {
  //send email
    return new Promise(function(resolve, reject) {
      var to = "";
      var subject = `Recibo de pago de ${store.name} # ${bill.metadata.billId}`;
      var body = `${bill.metadata.billId}`;
      for (var c=0;c<luka.profile.contacts.length;c++) {
        if (luka.profile.contacts[c].type == "email") {
          to = luka.profile.contacts[c].value;
        }
      }
      console.log(`sending email to ${to}`)

      var data = {
        to: `${to}`,
        subject: `${subject}`,
        textBody: ``,
        htmlBody: ``,
        metadata: {
            templateId: `${EMAIL_RECEIPT_TEMPLATE_NUMBER}`,
            date: `${bill.metadata.date}`,
            billid: `${bill.metadata.billId}`,
            accountid: `${bill.metadata.accountId}`,
            store: `${store.name}`,
            currency: `${bill.currency}`,
            amount: `${bill.value}`
        }
      }

      luka.sendEmail(data, function(response){
        if (response.error) {
          console.log(`Email sending error: ${response.error.name}`)
        }
        else {
          console.log(`Email sent`)
        }
      })
    });
  })
  .then(function(){
    return new Promise(function(resolve, reject) {

      console.log(`Finishing payment of bill ${billId}...`);

    })
  })
  .catch(function(followUpEvent){

    console.log(`Error processing ${billId}...`);

    if (followUpEvent != "") {
      console.log(`rejecting ${followUpEvent}`)
      options = {sessionId: `${session.sessionId}`};
      event = {name: `${followUpEvent}`,data: {}};
      facebookBot.addRequestEvents(event,options,session);
    }
    else {
      console.log(`rejecting ${followUpEvent}`)
      options = {sessionId: `${session.sessionId}`};
      event = {name: `bill-pay-fail`,data: {bill: `${billId}`, paymentMethod: paymentMethod}};
      facebookBot.addRequestEvents(event,options,session);
      res.render('pages/reject', {billId: billId, sessId: sessId, paymentMethod: paymentMethod, fb_page_url: FB_PAGE_URL});
    }
  });

})

app.get('/card/confirm/:billId/:sessId/:paymentMethod', (req, res) => {


  var billId = req.params.billId;
  var sessId = req.params.sessId;
  var paymentMethod = req.params.paymentMethod;
  var bill = undefined;
  var service = undefined;
  var store = undefined;
  var sequence = Promise.resolve();
  var event = undefined;
  var options = undefined;
  var success = require('url').parse(req.url,true).query.success; //&success=true|false

    session = sessions[sessId];
    //fix
    if (session == undefined) {
        session = new Object();
        session.sessionId = '{123}';
    }


    console.log(`Displaying confirmation or reject page for ${billId}, success=${success}...`);

    sequence = sequence.then(function(){
      //try to get a bill
      return new Promise(function(resolve, reject) {
          luka.getBill(billId, function(_bill){
            //check result
            if (!_bill) {
              console.log(`ERR: Cannot retrieve bill for ${billId}`);
              return reject('error')
            }
            else
              console.log(`Transaction ${billId}...`);
              bill = _bill;
              return resolve();
          });
      });
    })
    .then(function() {
    //get service details
      //try to get services
      console.log(`getting the service`)
      return new Promise(function(resolve, reject) {
          luka.getService(bill.metadata.service, function(_service){
            //check result
            if (!_service) {
              console.log(`ERR: Cannot retrieve services for filter ${bill.metadata.service}`);
              return reject('error')
            }
            else
              console.log(`we have it here ${JSON.stringify(_service)}`)
              service = _service;
              return resolve();
          });
      });

    })
    .then(function() {
    //get store details
      //try to get store
      console.log(`getting the store for ${service.org}`)
      return new Promise(function(resolve, reject) {
          luka.getStore(bill.metadata.store, function(_store){
            //check result
            if (!_store) {
              console.log(`ERR: Cannot retrieve store for filter ${bill.metadata.store}`);
              return reject('error')
            }
            else
              console.log(`we have it here ${JSON.stringify(_store)}`)
              store = _store;
              return resolve();
          });
      });
    })
    .then(function(){
      return new Promise(function(resolve, reject) {

        console.log(`Finishing payment of bill ${billId}...`);

        if (success == "true") {
          res.render('pages/confirm', {billId: bill.metadata.billId, accountId: bill.metadata.accountId, date:bill.metadata.date, store: store.name, currency: bill.currency, amount: bill.value, sessId: sessId, paymentMethod: paymentMethod, fb_page_url: FB_PAGE_URL});
        }
        else {
          res.render('pages/reject', {billId: billId, sessId: sessId, paymentMethod: paymentMethod, fb_page_url: FB_PAGE_URL});
        }
      })
    })
    .catch(function(followUpEvent){

      console.log(`Error processing ${billId}...`);

        console.log(`rejecting ${followUpEvent}`)
        options = {sessionId: `${me.session.sessionId}`};
        event = {name: `${followUpEvent}`,data: {}};
        facebookBot.addRequestEvents(event,options,me.session);
    });

})

app.listen((process.env.PORT || REST_PORT), () => {
    console.log('REST service ready on port ' + REST_PORT);
});

facebookBot.doSubscribeRequest();
