'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var service = undefined;
      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get services
        return new Promise(function(resolve, reject) {
            me.luka.getService(me.parameters.service, function(_service){
              //check result
              if (!_service) {
                console.log(`ERR: Cannot retrieve services for filter ${me.parameters.service}`);
                return reject('error')
              }
              else {
                console.log(`got service`);
                service = _service;
                return resolve();
              }
            });
        });
      })
      .then(function() {
      //save subscription

        return new Promise(function(resolve, reject) {
            console.log(`service subscribe ${me.parameters.service} ${me.parameters.accountId}`)
            me.luka.createSubscription (service.org, service._id, me.parameters.accountId, me.luka.profile._id, me.parameters.accountId, function(success){
              //check result
              if (!success) {
                console.log(`ERR: Cannot create subscription for service ${me.parameters.service}`);
                return reject('error')
              }
              else {
                callback({
                  source: `${me.source}`,
                  followupEvent: {
                    name: `service-subscribe-success`,
                    data: { service: `${service.name}`, account: `${me.parameters.accountId}`}
                  }
                });
              }
            });
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
