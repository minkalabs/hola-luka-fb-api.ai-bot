'use strict';

const _ = require('underscore')

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;

    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var stores = undefined;
      var services = undefined;
      var subscriptions = undefined;
      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get services
        return new Promise(function(resolve, reject) {
            me.luka.getStores('', function(_stores){
              //check result
              if (!_stores) {
                console.log(`ERR: Cannot retrieve stores for filter ${me.parameters.filter}`);
                return reject('error')
              }
              else {
                stores = _stores;
                console.log(`We have stores ${stores.length}`);
                return resolve();
              }
            });
        });
      })
      .then(function(){
        //try to get services
        return new Promise(function(resolve, reject) {
            me.luka.getServices('', function(_services){
              //check result
              if (!_services) {
                console.log(`ERR: Cannot retrieve services for filter ${me.parameters.filter}`);
                return reject('error')
              }
              else {
                services = _services;
                console.log(`We have services ${services.length}`);
                return resolve();
              }
            });
        });
      })
      .then(function(){
        //try to get subscriptions
        return new Promise(function(resolve, reject) {
            me.luka.getPersonSubscriptions(me.luka.profile._id, function(_subscriptions){
              //check result
              if (!_subscriptions) {
                console.log(`ERR: Cannot retrieve subscriptions for filter ${me.parameters.filter}`);
                return reject('error')
              }
              else {
                subscriptions = _subscriptions;
                console.log(`We have subscriptions ${subscriptions.length}`);
                return resolve();
              }
            });
        });
      })
      .then(function() {

        var currentStore = '';
        var currentService = '';

        return new Promise(function(resolve, reject) {

          if (subscriptions.length > 0) {
            var output = {source: `${me.source}`,data: {facebook: [] }}

            //prepare cards for every subscription
            for (var i=0;i<subscriptions.length;i++) {

              //get current service
              currentService = JSON.parse(JSON.stringify(_.filter(services,function(row){return row._id == `${subscriptions[i].metadata.item}`})));
              console.log(`currentService ${currentService}`);

              //get current store
              currentStore = JSON.parse(JSON.stringify(_.filter(stores,function(row){return row._id == `${currentService[0].org}`})));
              console.log(`currentStore ${currentStore}`);
              console.log(`subscriptions ${subscriptions[i]}`);

                output.data.facebook.push({
                  attachment: {
                    type: "template",
                    payload: {
                        template_type: "generic",
                        elements: [
                          {
                            //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                            title: `${currentService[0].metadata.name} `,
                            subtitle: `Nro. de cuenta ${subscriptions[i].metadata.accountId}`,
                            buttons: [
                              {
                                //"title": "Pay with Luka",
                                title: `Pagar`,
                                type: "postback",
                                payload: `pay subscription ${subscriptions[i]._id}`
                              },
                              {
                                //"title": "Pay with Luka",
                                title: `Borrar esto`,
                                type: "postback",
                                payload: `unsubscribe ${subscriptions[i]._id}`
                              }
                            ]
                          }
                        ]
                      }
                    }
                  });

            }
            return resolve(output);
          }
          else {
            //no subscriptions
            return reject(`subscription-list-empty`);
          }
        })
      })
      .then(function(output){
        callback(output);
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
