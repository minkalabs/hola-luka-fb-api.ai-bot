'use strict';

var rp = require('request-promise');
var dateFormat = require('dateformat');
const cJSON = require('circular-json')
const config = require('../../app.json')
const BOT_APP_URL = config.env.BOT_APP_URL.value;
const MINKA_API_URL = config.env.MINKA_API_URL.value;

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();
      var data = {};
      var service = undefined;
      var store = undefined;
      var bill = undefined;

      var event = undefined;
      var options = undefined;

      sequence = sequence.then(function() {
      //get service details
        //try to get services
        console.log(`getting the service`)
        return new Promise(function(resolve, reject) {
            me.luka.getServices(me.parameters.service, function(_service){
              //check result
              if (!_service) {
                console.log(`ERR: Cannot retrieve services for filter ${me.parameters.service}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_service)}`)
                service = _service[0];
                return resolve();
            });
        });

      })
      .then(function () {

        me.parameters.accountDescription = service.metadata.userInputDescription;
        me.parameters.minValue = service.metadata.minValue;
        me.parameters.maxValue = service.metadata.maxValue;
        console.log(`incompleteAction getting the service`)
        console.log(`accountDescription: ${me.parameters.accountDescription}`)
        //if amount is out of range reset it
        console.log(`accountDescription set before: ${isNaN(parseFloat(me.parameters.amount))} ${me.parameters.minValue} ${me.parameters.maxValue} ${me.parameters.amount}`)
        return new Promise(function(resolve, reject) {
          var infoText = " ";
          if (me.actionIncomplete || me.parameters.accountDescription == '' || me.parameters.amount == '' || isNaN(parseFloat(me.parameters.amount)) || parseFloat(me.parameters.amount)<parseFloat(me.parameters.minValue) || parseFloat(me.parameters.amount)>parseFloat(me.parameters.maxValue)) {
            //console.log(`${JSON.stringify(me.contexts)}`)
            //console.log(`${JSON.stringify(me.parameters)}`)
            if ((parseFloat(me.parameters.amount)<parseFloat(me.parameters.minValue) || parseFloat(me.parameters.amount)>parseFloat(me.parameters.maxValue)))
              {
                for (var c=0;c<me.contexts.length;c++) {
                  //me.contexts[c].parameters.amount.original = "";
                  me.contexts[c].parameters.amount = "";
                }
              me.parameters.amount = "";
              infoText = `El monto es incorrecto, por favor escriba un monto entre ${me.parameters.minValue} y ${me.parameters.maxValue} ${service.metadata.serviceCurrency}. `;
              }
              console.log(`accountDescription set after: ${infoText} ${isNaN(parseFloat(me.parameters.amount))} ${me.parameters.accountDescription} ${me.parameters.minValue} ${me.parameters.maxValue} ${me.parameters.amount}`)
              callback({
                source: `${me.source}`,
                contextOut: me.contexts,
                followupEvent: {
                  name: `service-pay`,
                  data: { infoText: `${infoText}`, service:`${me.parameters.service}`, accountDescription:`${me.parameters.accountDescription}`, amount: `${me.parameters.amount}`, account: `${me.parameters.account}`, minValue: `${me.parameters.minValue}`, maxValue: `${me.parameters.maxValue}` }
                }
              });
              return reject(``);
            }
            else {
              return resolve();
            }
          });
      })
      .then(function() {
      //get store details
        //try to get store
        console.log(`getting the store for ${service.org}`)
        return new Promise(function(resolve, reject) {
            me.luka.getStore(service.org, function(_store){
              //check result
              if (!_store) {
                console.log(`ERR: Cannot retrieve store for filter ${service.org}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_store)}`)
                store = _store;
                return resolve();
            });
        });
      })
      .then(function() {
      //verify new bill

        return new Promise(function(resolve, reject) {
          //respond to user that we are verifying the account

          options = {sessionId: `${me.session.sessionId}`};
          event = {name: 'service-pay-verify-bill-info',data: {}};
          me.bot.addRequestEvents(event,options,me.session);

          var options = {
              method: 'POST',
              uri: `${BOT_APP_URL}/gateway/verify`,
              body:  {
                source: `${me.luka.profile.lukatag}`,
                destination: `${store.lukatag}`,
                value: `${me.parameters.amount}`,
                currency: `${service.amount[0].currency}`,
                charges: {

                },
                metadata: {
                    billId: "",
                    created: "",
                    status: "",
                    comment: "",
                    name:``,
                    paymentGateway: "",
                    paymentMethods: "",
                    paymentMethod: "",
                    accountId: `${me.parameters.account}`,
                    code: "",
                    subscription: "",
                    service: `${service._id}`,
                    store: `${store._id}`,
                    dueDate: "",
                    transfers: {},
                    error: ""
               }
              },
              json: true // Automatically stringifies the body to JSON
          };

          rp(options)
            .then(function(_bill){

              bill = JSON.parse(JSON.stringify(_bill));
              console.log(`verifying bill  ${JSON.stringify(_bill)} ${bill.metadata.billId} ${bill.status}`)
              if (bill.metadata.billId == undefined) {
                //error returning
                me.luka.error = bill.metadata.error;
                throw new Error(me.luka.error);
              }
              else if (bill.status == 'DRAFT' || bill.status == 'INVALID') {
                //account doesn't exist
                data = {service: `${service.name}`, amount: `${me.parameters.amount}`, currency: `${service.serviceCurrency}`}
                return reject('service-pay-verify-fail');
                //return resolve();
              }
              else {
                //return result
                return resolve();
                }
              })
            .catch(function(err){
              console.log(`Error verifying bill ${JSON.stringify(err)}`);
              return reject('bill-pay-fail');
              //return resolve();
            });

        });
      })
      .then(function() {
      //save bill
        return new Promise(function(resolve, reject) {
          console.log(`saving bill ${bill.metadata.billId}... /transfer`)
          data.data = bill;
          me.luka.createTransaction(data, function(trxId){
            if (!trxId) {
              console.log(`Error creating a transfer`)
              options = {sessionId: `${me.session.sessionId}`};
              event = {name: `error`,data: {}};
              me.bot.addRequestEvents(event,options,me.session);

            }
            else {
              console.log(`Transfer created! trxId = ${trxId}`)
              options = {sessionId: `${me.session.sessionId}`};
              event = {name: `bill-pay-payment-methods`,data: {bill: `${trxId}`}};
              me.bot.addRequestEvents(event,options,me.session);
            }
          })
        });
      })
      .catch(function(followUpEvent){
        if (followUpEvent != "") {
        console.log(`rejecting ${followUpEvent} ${JSON.stringify(data)}`)
          options = {sessionId: `${me.session.sessionId}`};
          event = {name: `${followUpEvent}`,data: {}};
          me.bot.addRequestEvents(event,options,me.session);
        }
      });


  }


}

module.exports = ActionObject;
