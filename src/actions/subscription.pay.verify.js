'use strict';

var rp = require('request-promise');
var dateFormat = require('dateformat');
const cJSON = require('circular-json')
const config = require('../../app.json')
const BOT_APP_URL = config.env.BOT_APP_URL.value;
const MINKA_API_URL = config.env.MINKA_API_URL.value;

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();
      var data = {};

      var subscription = undefined;
      var service = undefined;
      var store = undefined;
      var bill = undefined;

      var event = undefined;
      var options = undefined;

      //if actionIncomplete then retrieve details
      if (false) {
        console.log(`contexts ${JSON.stringify(me.contexts)}`)
        console.log(`parameters ${JSON.stringify(me.parameters)}`)
        sequence = sequence.then(function() {
        //get service details
          //try to get services
          console.log(`getting the subscription ${me.parameters.subscription}`)
          return new Promise(function(resolve, reject) {
              me.luka.getSubscription(me.parameters.subscription, function(_subscription){
                //check result
                if (!_subscription) {
                  console.log(`ERR: Cannot retrieve subscription for filter ${me.parameters.subscription}`);
                  return reject('error')
                }
                else
                  console.log(`we have subscription ${JSON.stringify(_subscription)}`)
                  subscription = _subscription;
                  return resolve();
              });
          });

        })
        .then(function() {
        //get service details
          //try to get services
          console.log(`incompleteAction getting the service`)
          console.log(`accountDescription: ${me.parameters.accountDescription}`)

              me.luka.getServices(subscription.metadata.item, function(_service){
                //check result
                if (!_service) {
                  console.log(`ERR: Cannot retrieve services for filter ${me.parameters.service}`);
                  callback({
                    source: `${me.source}`,
                    followupEvent: {
                      name: `error`,
                      data: {}
                    }
                  });
                }
                else {
                  console.log(`incompleteAction we have a service ${JSON.stringify(_service)} `)
                  me.parameters.accountDescription = _service[0].metadata.user_input[0].name;
                  //if amount is out of range reset it
                  callback({
                    source: `${me.source}`,
                    contextOut: me.contexts,
                    followupEvent: {
                      name: `subscription-pay-verify`,
                      data: { subscription:`${me.parameters.subscription}`, accountDescription:`${me.parameters.accountDescription}` }
                    }
                  });
                }
              });
        })
      }
      else {
      sequence = sequence.then(function() {
      //get service details
        //try to get services
        console.log(`getting the subscription ${me.parameters.subscription}`)
        return new Promise(function(resolve, reject) {
            me.luka.getSubscription(me.parameters.subscription, function(_subscription){
              //check result
              if (!_subscription) {
                console.log(`ERR: Cannot retrieve subscription for filter ${me.parameters.subscription}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_subscription)}`)
                subscription = _subscription;
                return resolve();
            });
        });

      })
      .then(function() {
      //get service details
        //try to get services
        console.log(`getting the service`)
        return new Promise(function(resolve, reject) {
            me.luka.getService(subscription.metadata.item, function(_service){
              //check result
              if (!_service) {
                console.log(`ERR: Cannot retrieve services for filter ${subscription.metadata.item}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_service)}`)
                service = _service;
                return resolve();
            });
        });

      })
      .then(function() {
      //get store details
        //try to get store
        console.log(`getting the store for ${service.org}`)
        return new Promise(function(resolve, reject) {
            me.luka.getStore(service.org, function(_store){
              //check result
              if (!_store) {
                console.log(`ERR: Cannot retrieve store for filter ${service.org}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_store)}`)
                store = _store;
                return resolve();
            });
        });
      })
      .then(function() {
      //verify new bill

        return new Promise(function(resolve, reject) {
          //respond to user that we are verifying the account

          console.log(`ACCOUNT ID IS ${subscription.metadata.accountId}`)

          options = {sessionId: `${me.session.sessionId}`};
          event = {name: 'service-pay-verify-account-info',data: {}};
          me.bot.addRequestEvents(event,options,me.session);

          var options = {
              method: 'POST',
              uri: `${BOT_APP_URL}/gateway/verify`,
              body: {
                source: `${me.luka.profile.lukatag}`,
                destination: `${store.lukatag}`,
                value: "",
                currency: `${service.amount[0].currency}`,
                charges: {

                },
                metadata: {
                    billId: "",
                    created: "",
                    status: "",
                    comment: "",
                    name:``,
                    paymentGateway: "",
                    paymentMethods: "",
                    paymentMethod: "",
                    accountId: `${subscription.metadata.accountId}`,
                    code: "",
                    subscription: `${subscription._id}`,
                    service: `${service._id}`,
                    store: `${store._id}`,
                    dueDate: "",
                    transfers: {},
                    error: ""
               }
              },
              json: true // Automatically stringifies the body to JSON
          };

          rp(options)
            .then(function(_bill){
              bill = JSON.parse(JSON.stringify(_bill));
              console.log(`verifying bill  ${JSON.stringify(_bill)} ${bill.metadata.billId} ${bill.status}`)

              if (bill.metadata.billId == undefined) {
                //error returning
                me.luka.error = bill.metadata.error;
                throw new Error(me.luka.error);
              }
              else if (bill.status == 'DRAFT' || bill.status == 'INVALID') {
                //account doesn't exist
                data = {service: `${service.name}`}
                return reject('service-pay-verify-fail');
                //return resolve();
              }
              else {
                //return result
                return resolve();
                }
              })
            .catch(function(err){
              console.log(`Error verifying bill ${JSON.stringify(err)}`);
              return reject('bill-pay-fail');
              //return resolve();
            });


        });
      })
      .then(function() {
      //save bill
        return new Promise(function(resolve, reject) {

          options = {sessionId: `${me.session.sessionId}`};
          event = {name: `bill-pay-info`,data: {info: `${bill.metadata.description}`}};
          me.bot.addRequestEvents(event,options,me.session);

          console.log(`calling subscription-pay {subscription: ${bill.metadata.subscription}, minValue: ${service.metadata.min_value}, maxValue: ${service.metadata.max_value}, accountDescription: ${service.metadata.user_input[0].name}, infoText: ""}`)

          options = {sessionId: `${me.session.sessionId}`};
          event = {name: `subscription-pay`,data: {subscription: `${bill.metadata.subscription}`, minValue: `${service.metadata.min_value}`, maxValue: `${service.metadata.max_value}`, accountDescription: `${service.metadata.user_input[0].name}`, infoText: ` `}};
          me.bot.addRequestEvents(event,options,me.session);

        });
      })
      .catch(function(followUpEvent){
        console.log(`rejecting ${followUpEvent} ${JSON.stringify(data)}`)
          options = {sessionId: `${me.session.sessionId}`};
          event = {name: `${followUpEvent}`,data: {}};
          me.bot.addRequestEvents(event,options,me.session);
      });
    }
  }


}

module.exports = ActionObject;
