'use strict';

var rp = require('request-promise');
var dateFormat = require('dateformat');
const cJSON = require('circular-json')
const config = require('../../app.json')
const BOT_APP_URL = config.env.BOT_APP_URL.value;
const MINKA_API_URL = config.env.MINKA_API_URL.value;

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated || me.luka == undefined) {
        return callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var bill = undefined;
      var sequence = Promise.resolve();

      var event = undefined;
      var options = undefined;

      sequence = sequence.then(function(){
        //try to get a bill
        return new Promise(function(resolve, reject) {
            me.luka.getBill(me.parameters.bill, function(_bill){
              //check result
              if (!_bill) {
                console.log(`ERR: Cannot retrieve bill for ${me.parameters.billId}`);
                return reject('error')
              }
              else
                bill = _bill;
                return resolve();
            });
        });
      })
      .then(function(){
        //try to get pagosnet transaction
        return new Promise(function(resolve, reject) {
        console.log(`${JSON.stringify(bill)}`)

        //now
        var d = new Date();
        //24h timeframe to pay
        var t = new Date(d.getTime()+1000*60*60*24);

        console.log(JSON.stringify({
              registroPlanes: [
              {
                datos: {
                  nombreComprador: `${me.luka.profile.firstname} ${me.luka.profile.lastname}`,
                  documentoIdentidadComprador: `${me.luka.profile._id}`,
                  codigoComprador: `${me.luka.profile._id}`,
                  fecha: `${dateFormat(d,"yyyymmdd")}`,
                  hora: `${dateFormat(d,"hhmmss")}`,
                  fechaVencimiento: `${dateFormat(t,"yyyymmdd")}`,
                  horaVencimiento: `${dateFormat(t,"hhmmss")}`,
                  correoElectronico: `${me.luka.profile.email}`,
                  moneda: "BS",
                  codigoRecaudacion: `${bill.metadata.billId}`,
                  descripcionRecaudacion: `Luka Payment of bill ${bill.metadata.billId}`,
                  categoriaProducto: "1",
                  precedenciaCobro: "S",
                  planillas: [
                    {
                      numeroPago: 1,
                      montoPago: `${parseFloat(bill.amount.value).toFixed(1)}`,
                      montoCreditoFiscal: 0,
                      nombreFactura: `${me.luka.profile.firstname} ${me.luka.profile.lastname}`,
                      nitFactura: `${bill.metadata.billId}`,
                      descripcion: `Luka Payment of bill ${bill.metadata.billId}`
                    }
                  ]
                },
                idComercio: 7,
                cuenta: "wstransworld",
                password: "Wstransworld2017 ",
                datosItemsDePlanillas:[]
              }
              ],
              codIntegrador: 134,
              codAgrupacion: `${bill.metadata.billId}`,
              cuenta: "wscncei",
              password: "Wscncei2017",

               datosTarjetaHabiente:{
                nombreComprador: `${me.luka.profile.firstname}`,
                apellidoComprador: `${me.luka.profile.lastname}`,
                correo: `${me.luka.profile.email}`,
                direccion: `${me.luka.profile.location[0].address}`,
                ciudad: `${me.luka.profile.location[0].city}`,
                departamento: `${me.luka.profile.location[0].state}`,
                pais: `${me.luka.profile.location[0].country}`,
                telefono: `${me.luka.profile.phone}`
              }
            }))


        var options = {
            method: 'POST',
            uri: `https://test.sintesis.com.bo/RestPagosnet/pagosnet-integrators/recaudaciones/agrupaciones`,
            body: {
              registroPlanes: [
              {
                datos: {
                  nombreComprador: `${me.luka.profile.firstname} ${me.luka.profile.lastname}`,
                  documentoIdentidadComprador: `${me.luka.profile._id}`,
                  codigoComprador: `${me.luka.profile._id}`,
                  fecha: `${dateFormat(d,"yyyymmdd")}`,
                  hora: `${dateFormat(d,"hhmmss")}`,
                  fechaVencimiento: `${dateFormat(t,"yyyymmdd")}`,
                  horaVencimiento: `${dateFormat(t,"hhmmss")}`,
                  correoElectronico: `${me.luka.profile.email}`,
                  moneda: "BS",
                  codigoRecaudacion: `${bill.metadata.billId}`,
                  descripcionRecaudacion: `Luka Payment of bill ${bill.metadata.billId}`,
                  categoriaProducto: "1",
                  precedenciaCobro: "S",
                  planillas: [
                    {
                      numeroPago: 1,
                      montoPago: `${parseFloat(bill.amount.value).toFixed(1)}`,
                      montoCreditoFiscal: 0,
                      nombreFactura: `${me.luka.profile.firstname} ${me.luka.profile.lastname}`,
                      nitFactura: `${bill.metadata.billId}`,
                      descripcion: `Luka Payment of bill ${bill.metadata.billId}`
                    }
                  ]
                },
                idComercio: 560,
                //cuenta: "wstransworld",
                cuenta: "wsluka1",
                //password: "Wstransworld2017 ",
                password: "Wsluka12017 ",
                datosItemsDePlanillas:[]
              }
              ],
              codIntegrador: 559,
              codAgrupacion: `${bill.metadata.billId}`,
              //cuenta: "wscncei",
              cuenta: "wsintluka",
              //password: "Wscncei2017 ",
              password: "Wsintluka2017 ",

               datosTarjetaHabiente:{
                nombreComprador: `${me.luka.profile.firstname}`,
                apellidoComprador: `${me.luka.profile.lastname}`,
                correo: `${me.luka.profile.email}`,
                direccion: `${me.luka.profile.location[0].address}`,
                ciudad: `${me.luka.profile.location[0].city}`,
                departamento: `${me.luka.profile.location[0].state}`,
                pais: `${me.luka.profile.country}`,
                telefono: `${me.luka.profile.phone}`
              }
            },
            json: true // Automatically stringifies the body to JSON
        };
        rp(options)
          .then(function(body){
            body = JSON.parse(JSON.stringify(body));
            if (body.codigoError == undefined || body.codigoError != "0") {
              //pagosent returned an error
              throw new Error(body.descripcionError);
            }
            else {
              //user logged in
              console.log(`pagosent auth success: ${body.idTransaccion}`);
              //return access_token
              return resolve(body.idTransaccion);
              }
            })
          .catch(function(err){

            console.log(`PagosNet error: ${err}`);
            return reject('error');
          });
        });
      })
      .then(function(idTransaccion) {
        var output = {source: `${me.source}`,data: {facebook: [] }}
        //prepare cards for payment options
        output.data.facebook.push({
          attachment: {
            type: "template",
            payload: {
                template_type: "generic",
                elements: [
                  {
                    //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                    title: `${bill.metadata.name}`,
                    subtitle: `${bill.amount.value} ${bill.amount.currency}\n#${bill.metadata.billId}`,
                    buttons: [
                      {
                        type:"web_url",
                        title:"Pagar con tarjeta",
                        url:`https://test.sintesis.com.bo/payment/#/pay?entidad=559&ref=${idTransaccion}&red=${BOT_APP_URL}/card/confirm/${bill.metadata.billId}/${me.session.sessionId}/PAGOSNET`,
                        webview_height_ratio: "full",
                        messenger_extensions: true,
                        fallback_url: `https://test.sintesis.com.bo/payment/#/pay?entidad=559&ref=${idTransaccion}&red=${BOT_APP_URL}/card/confirm/${bill.metadata.billId}/${me.session.sessionId}/PAGOSNET`
                      },
                      {
                        //"title": "Pay with Luka",
                        title: `Cancelar`,
                        type: "postback",
                        payload: `cancelar`
                      }
                    ]
                  }
                ]
              }
            }
          });
        callback(output);
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
