'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated || me.luka == undefined) {
        return callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var bill = undefined;

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
      //get bill details
        return new Promise(function(resolve, reject) {
            me.luka.getBill(me.parameters.bill, function(_bill){
              //check result
              if (!_bill) {
                console.log(`ERR: Cannot retrieve bill for filter ${me.parameters.bill}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_bill)}`)
                bill = _bill[0];
                return resolve();
            });
        });
      })
      .then(function(){

        //TO DO: generate pdf and offer download
        var options = {sessionId: `${me.session.sessionId}`};
        var event = {name: `error`,data: {}};
        me.bot.addRequestEvents(event,options,me.session);

      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
