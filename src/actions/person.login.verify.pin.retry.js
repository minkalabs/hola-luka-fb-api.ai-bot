'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.fbid = session.fbid;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {

      var me = this;

      console.log(`parameters ${JSON.stringify(this.parameters)}`);
      console.log(`contexts ${JSON.stringify(this.contexts)}`);

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to verify PIN
        return new Promise(function(resolve, reject) {
            me.session.context.luka.verifyPIN(me.parameters.phone, me.parameters.pin, function(success){
              //check PIN
              if (!success) {
                console.log(`ERR: PIN invalid for ${me.parameters.lukatag}`);
                callback({
                  source: `${me.source}`,
                  followupEvent: {
                    name: `person-login-verify-pin-retry`,
                    data: { phone: me.parameters.phone, lukatag: me.parameters.lukatag}
                  }
                });
              }
              else
                return resolve();
            });
        });
      })
      .then(function(){
        //try to retrieve person
        return new Promise(function(resolve, reject) {

          me.session.context.luka.getPerson(me.parameters.lukatag, function(profile){
            if (profile.lukatag == undefined) {
              console.log(`ERR: Cannot retrieve ${me.parameters.lukatag}`);
              return reject('person-login-invalid-lukatag')
            }
            else {
                return resolve(profile);
              }
          })
        });
      })
      .then(function(){
        //PIN valid
        console.log(`User ${me.parameters.lukatag} validated.`);
        //set authenticated flag
        me.luka.authenticated = true;
        me.luka.profile = profile;
        //add a channel to person profile
        var channel = { id: me.session.fbid, status: "VERIFIED", metadata: { type: me.source, name: me.source, fbid: me.session.fbpageid}}
        //get phone
        var phone = "";
        console.log(`Finding a phone..`);
        for (var c=0;c<profile.contacts.length;c++) {
          if (profile.contacts[c].type=="phone") {
            phone = profile.contacts[c].value;
          }
        }

        me.luka.genericPost(`POST`,`/channel/${me.parameters.lukatag}`,channel, function(result){
          if (result.response == false) {
            return reject('invalid-auth');
          }
          else {
            callback({
              source: `${me.source}`,
              followupEvent: {
                name: `person-login-confirm`,
                data: { phone: phone, lukatag: me.parameters.lukatag}
              }
            });

          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        }
       );
      });



    }
}
module.exports = ActionObject;
