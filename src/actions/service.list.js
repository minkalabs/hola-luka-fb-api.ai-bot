'use strict';

const config = require('../../app.json')
const BOT_APP_URL = config.env.BOT_APP_URL.value;

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;

    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      console.log(`in service.list`)
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var services = undefined;
      var subscriptions = undefined;
      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get services
        return new Promise(function(resolve, reject) {
            me.luka.getServices(me.parameters.filter, function(_services){
              //check result
              if (!_services) {
                console.log(`ERR: Cannot retrieve services for filter ${me.parameters.filter}`);
                return reject('error')
              }
              else {
                services = _services;
                return resolve();
              }
            });
        });
      })
      .then(function(){
        //try to get subscriptions
        return new Promise(function(resolve, reject) {
            me.luka.getPersonSubscriptions(me.luka.profile._id, function(_subscriptions){
              //check result
              if (!_subscriptions) {
                console.log(`ERR: Cannot retrieve subscriptions for filter ${me.parameters.filter}`);
                return reject('error')
              }
              else {
                subscriptions = _subscriptions;
                console.log(`We have subscriptions ${subscriptions.length}`);
                return resolve();
              }
            });
        });
      })
      .then(function() {

        return new Promise(function(resolve, reject) {

          if (services.length > 0) {
            var output = {source: `${me.source}`,data: {facebook: [] }}
            //prepare cards for every service
            for (var i=0;i<services.length;i++) {
                //add general button
                console.log(`Service nr. ${i} ${JSON.stringify(services[i])}`);
                var button_title = '';
                switch(services[i].metadata.type) {
                  case 'TOPUP':
                    button_title = 'Recarga';
                  break;
                  case 'PREPAID':
                    button_title = 'Pagar';
                  break;
                  case 'POSTPAID':
                    button_title = 'Pagar';
                  break;
                  default:
                    button_title = 'Pagar';
                  break;
                }
                output.data.facebook.push({
                  attachment: {
                    type: "template",
                    payload: {
                        template_type: "generic",
                        elements: [
                          {
                            //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                            title: `${services[i].metadata.name}`,
                            subtitle: `${services[i].metadata.description}`,
                            image_url: `${BOT_APP_URL}${services[i].metadata.logo}`,
                            buttons: [
                              {
                                //"title": "Pay with Luka",
                                title: `${button_title}`,
                                type: "postback",
                                payload: `verify service ${services[i].metadata.name}`
                              }
                            ]
                          }
                        ]
                      }
                    }
                  });
                //add a list a of subscriptions
                var buttons_limit = 0;
                for (var s=0;s<subscriptions.length;s++) {
                  if (subscriptions[s].metadata.item == services[i]._id && buttons_limit<2) {
                    output.data.facebook[0].attachment.payload.elements[0].buttons.push(
                      {
                        title: `${button_title} ${subscriptions[s].metadata.accountId}`,
                        type: "postback",
                        payload: `pay subscription ${subscriptions[s]._id}`
                      }
                    );
                    buttons_limit++;
                  }
                }
            }
            return resolve(output);
          }
          else {
            //no pending transactions
            return reject(`service-list-empty`);
          }
        })
      })
      .then(function(output){
        callback(output);
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
