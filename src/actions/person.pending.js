'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get pending transactions
        return new Promise(function(resolve, reject) {
            me.luka.getPendingTransactions(me.luka.profile.lukatag, function(transactions){
              //check result
              if (!transactions) {
                console.log(`ERR: Cannot retrieve pending transactions for ${me.luka.profile.lukatag}`);
                return reject('error')
              }
              else
                return resolve(transactions);
            });
        });
      })
      .then(function(transactions) {

        if (transactions.length > 0) {
          var output = {source: `${me.source}`,data: {facebook: [] }}
          console.log(`pending1 ${JSON.stringify(output)}`)
          //prepare cards for every pending bill
          for (var i=0;i<transactions.length;i++) {
            output.data.facebook.push({
              attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [
                      {
                        //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                        title: `#${i+1}. ${transactions[i]._links.destination} `,
                        subtitle: `${transactions[i].metadata.description}\n${transactions[i].created}`,
                        buttons: [
                          {
                            //"title": "Pay with Luka",
                            title: `${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                            type: "postback",
                            payload: `PAY_PENDING ${transactions[i]._id}`
                          }
                        ]
                      }
                    ]
                  }
                }
              });
          }
          console.log(`pending2 ${JSON.stringify(output)}`)
          callback(output);
        }
        else {
          //no pending transactions
          callback({
            source: `${me.source}`,
            followupEvent: {
              name: `person-pending-empty`,
              data: { }
            }
          });
        }
    })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
