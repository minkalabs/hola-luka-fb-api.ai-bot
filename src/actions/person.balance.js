'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        return callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get balance
        return new Promise(function(resolve, reject) {
            me.luka.getBalance(me.luka.profile.lukatag, function(result){
              //check result
              if (!result) {
                console.log(`ERR: Cannot retrieve balance for ${me.parameters.lukatag}`);
                return reject('error')
              }
              else {
                console.log(`Balance for ${me.luka.profile.lukatag}`);
                return resolve(result);
              }
            });
        });
      })
      .then(function(wallet){

        callback({
          source: `${me.source}`,
          followupEvent: {
            name: 'person-balance-response',
            data: { balance: `${wallet.balance} LCO` }
          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
  }
module.exports = ActionObject;
