'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var rejectData = undefined;
      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to find receiver
        return new Promise(function(resolve, reject) {
            me.luka.getPerson(me.parameters.from, function(result){
              //check result
              if (!result) {
                console.log(`ERR: Cannot find receiver ${me.parameters.to}`);
                rejectData = {};
                return reject('person-transfer-invalid-lukatag')
              }
              else
                return resolve();
            });
        });
      })
      .then(function(success){
        //try to send payment
        return new Promise(function(resolve, reject) {
            me.luka.sendPayment(me.parameters.from, me.luka.profile.lukatag, me.parameters.amount, me.parameters.currency, me.parameters.note, function(txid){
              //check result
              if (!txid) {
                console.log(`ERR: Cannot request payment from ${me.parameters.from}`);
                return reject('person-transfer-reject')
              }
              else
                return resolve(txid);
            });
        });
      })
      .then(function(){
        return callback({
          source: `${me.source}`,
          followupEvent: {
            name: `person-transfer-confirm`,
            data: {  }
          }
        });
      })
      .catch(function(followUpEvent){
        return callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: rejectData
          }
        });
      });
    }
}
module.exports = ActionObject;
