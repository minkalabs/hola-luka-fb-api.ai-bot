'use strict';

var S = require('string');
const _ = require('underscore')


class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;

    }
    //main method
    execute(callback) {

      var me = this;
      var profile = undefined;
      var phone = "";

      var sequence = Promise.resolve();
      sequence = sequence.then(function(){
        //try to retrieve person
        return new Promise(function(resolve, reject) {
          console.log(`auth ${me.parameters.lukatag}`)
          console.log(`auth s ${me.session.context.luka}`)
          me.session.context.luka.getPerson(me.parameters.lukatag, function(_profile){
          console.log(`auth ${JSON.stringify(_profile)}`)
            if (_profile.lukatag == undefined) {
              console.log(`ERR: Cannot retrieve ${me.parameters.lukatag}`);
              return reject('person-login-invalid-lukatag')
            }
            else {
                profile = _profile;
                return resolve();
              }
          })
        });
      })
      .then(function(){
        //try send pin
        return new Promise(function(resolve, reject) {
          //try send PIN
          console.log(`Finding a phone..`);
          for (var c=0;c<profile.contacts.length;c++) {
            if (profile.contacts[c].type=="phone") {
              phone = profile.contacts[c].value;
            }
          }

          console.log(`Sending PIN to ${phone} for ${profile.lukatag}`);
          me.session.context.luka.sendPIN(phone, function(success){
          if (!success) {
            console.log(`ERR: Cannot send PIN to ${phone}`);
            return reject('person-login-send-pin-failed')
          }
          else {
            console.log(`PIN sent to ${phone} for ${profile.lukatag}.`);
            return resolve();
          }
          });
        });

      })
      .then(function(){
        //ask user to enter pin
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `person-login-verify-pin`,
            data: { phone: `${phone}`, lukatag: profile.lukatag }
          }
        }
       );
      })
      .catch(function(followUpEvent){

        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        }
       );
      });



    }
}
module.exports = ActionObject;
