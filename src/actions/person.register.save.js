'use strict';

var S = require('string');

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get balance
        return new Promise(function(resolve, reject) {
          var countryCode = '';
          console.log(`registration countryCode ${S(me.parameters.country).toLowerCase()}`)
          switch(S(me.parameters.country).toLowerCase().s) {
            case "bolivia":
              countryCode = "BO";
            break;
            case "colombia":
              countryCode = "CO";
            break;
            default:
              countryCode = "BO";
            break;
          }
            me.luka.savePerson(new Date().getTime(), me.parameters.phone, me.parameters.lukatag, me.parameters.email, me.parameters.firstname, me.parameters.lastname, me.parameters.address, me.parameters.city, me.parameters.state, me.parameters.zip, me.parameters.country, countryCode, me.session.fbid, function(result){
              //check result
              if (!result) {
                console.log(`ERR: Cannot save profile ${me.parameters.phone}, ${me.parameters.lukatag}, ${me.parameters.email}, ${me.parameters.firstname}, ${me.parameters.lastname}, ${me.parameters.address}, ${me.parameters.city}, ${me.parameters.state}, ${me.parameters.zip}, ${me.parameters.country}`);
                return reject('error')
              }
              else
                console.log('Profile saved!');
                return resolve();
            });
        });
      })
      .then(function(){

        callback({
          source: `${me.source}`,
          followupEvent: {
            name: 'person-register-confirm',
            data: { }
          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
