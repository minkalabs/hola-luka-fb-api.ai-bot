'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();

      sequence = sequence.then(function() {
      //save subscription

        return new Promise(function(resolve, reject) {
            var channelId = 0;
            console.log(`Finding a channel to delete`);
            for (var c=0;c<me.luka.profile.contacts.length;c++) {
              if (me.luka.profile.contacts[c].value==me.session.fbid) {
                channelId = me.luka.profile.contacts[c]._id;
                console.log(`Found a channel to delete ${channelId}`);
              }
            }
            if (channelId == 0)
              console.log(`Cannot find a channel to delete`);
            me.luka.deleteChannel(me.luka.profile.lukatag, channelId, function(success){
              //check result
              if (!success) {
                console.log(`ERR: Cannot delete channel for ${me.session.fbid}`);
                return reject('not-authenticated')
              }
              else {
                //delete a profile in luka object
                delete me.luka.profile;
                console.log(`SUCCESS: Luka profile deleted`);
                me.luka.authenticated = false;
                callback({
                  source: `${me.source}`,
                  followupEvent: {
                    name: `person-logout-success`,
                    data: {}
                  }
                });
              }
            });
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
