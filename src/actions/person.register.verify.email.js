'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //verify if person exists
        return new Promise(function(resolve, reject) {
            me.luka.getPerson(me.parameters.email, function(result){
              //check result
              if (result) {
                console.log(`ERR: lukatag exists ${me.parameters.lukatag}`);
                return reject('person-register-verify-email')
              }
              else
                return resolve();
            });
        });
      })
      .then(function(){
        //get facebook profile and send user details to intent
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: 'person-register-profile',
            data: {}
          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
