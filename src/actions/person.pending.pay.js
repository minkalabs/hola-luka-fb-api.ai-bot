'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var rejectData = undefined;
      var sequence = Promise.resolve();

      sequence = sequence.then(function(txid){
        //try to get senders ("my") balance
        return new Promise(function(resolve, reject) {
            me.luka.verifyTransaction(me.parameters.txid, function(result){
              //check result
              if (!result) {
                console.log(`ERR: Cannot verify txid ${me.parameters.txid}`);
                return reject('person-transfer-reject')
              }
              else
                return resolve();
            });
        });
      })
      .then(function(){
        return callback({
          source: `${me.source}`,
          followupEvent: {
            name: `person-transfer-confirm`,
            data: {  }
          }
        });
      })
      .catch(function(followUpEvent){
        return callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: rejectData
          }
        });
      });
    }
}
module.exports = ActionObject;
