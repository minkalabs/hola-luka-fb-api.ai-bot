'use strict';

var rp = require('request-promise');
var dateFormat = require('dateformat');
const cJSON = require('circular-json')
const config = require('../../app.json')
const BOT_APP_URL = config.env.BOT_APP_URL.value;
const MINKA_API_URL = config.env.MINKA_API_URL.value;

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated || me.luka == undefined) {
        return callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var bill = undefined;
      var sequence = Promise.resolve();

      var event = undefined;
      var options = undefined;

      sequence = sequence.then(function(){
        //try to get a bill
        return new Promise(function(resolve, reject) {
            me.luka.getBill(me.parameters.bill, function(_bill){
              //check result
              if (!_bill) {
                console.log(`ERR: Cannot retrieve bill for ${me.parameters.billId}`);
                return reject('error')
              }
              else
                bill = _bill;
                return resolve();
            });
        });
      })
      .then(function(){
        //try to get payment URL link
        return new Promise(function(resolve, reject) {
          console.log(`trying to get a paymentURL for this bill ${JSON.stringify(bill)}`)

          var Adapter = require('../gateway_adapter.js');
          var adapter = new Adapter();

          adapter.getPaymentURL(bill, function(payment_url){
            //check result
            if (!payment_url) {
              console.log(`ERR: Cannot retrieve payment_url for ${me.parameters.billId}`);
              return reject('error')
            }
            else
            console.log(`Payment_url for ${payment_url}`);
            return resolve(payment_url)
          });
        });
      })
      .then(function(payment_url) {
        //prepare a payment_url with a return page parameter
        var paymentUrl = "";
        paymentUrl = payment_url;
        payment_url = paymentUrl.replace(`[RED]`,`${BOT_APP_URL}/card/confirm/${bill._id}/${me.session.sessionId}/ATC&txId=${bill._id}&sessionId=${me.session.sessionId}`)
        console.log(`payment_url ${payment_url}`);
        var output = {source: `${me.source}`,data: {facebook: [] }}
        //prepare cards for payment options
        output.data.facebook.push({
          attachment: {
            type: "template",
            payload: {
                template_type: "generic",
                elements: [
                  {
                    title: `${bill.metadata.name}`,
                    subtitle: `Monto a pagar ${bill.value} ${bill.currency}\n#${bill.metadata.billId}`,
                    buttons: [
                      {
                        type:"web_url",
                        title:"Pagar con tarjeta",
                        url:`${payment_url}`,
                        webview_height_ratio: "full",
                        messenger_extensions: true,
                        fallback_url: `${payment_url}`
                      },
                      {
                        title: `Cancelar`,
                        type: "postback",
                        payload: `cancel`
                      }
                    ]
                  }
                ]
              }
            }
          });
        callback(output);
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
