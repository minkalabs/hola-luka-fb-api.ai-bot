'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
        return callback({
          source: `${this.source}`,
          followupEvent: {
            contextOut: {},
            name: 'default-cancel-info',
            data: {}
          }
        });
    }
}

module.exports = ActionObject;
