'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //verify pin
        return new Promise(function(resolve, reject) {
            me.luka.verifyPIN(me.parameters.phone, me.parameters.pin, function(result){
              //check result
              if (!result) {
                console.log(`ERR: invalid PIN ${me.parameters.phone}`);
                return reject('person-register-verify-phone')
              }
              else {
                return resolve();
              }
            });
        });
      })
      .then(function(){

        callback({
          source: `${me.source}`,
          followupEvent: {
            name: 'person-register-lukatag',
            data: {}
          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
