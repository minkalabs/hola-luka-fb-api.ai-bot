'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var service = undefined;
      var sequence = Promise.resolve();

      console.log(`let's delete subscription ${me.parameters.subscription}`)

      sequence = sequence.then(function() {
      //delete subscription

        return new Promise(function(resolve, reject) {

            me.luka.deleteSubscription (me.parameters.subscription, function(success){
              //check result
              if (!success) {
                console.log(`ERR: Cannot delete subscription ${me.parameters.subscription}`);
                return reject('error')
              }
              else {
                callback({
                  source: `${me.source}`,
                  followupEvent: {
                    name: `subscription-delete-success`,
                    data: {}
                  }
                });
              }
            });
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
