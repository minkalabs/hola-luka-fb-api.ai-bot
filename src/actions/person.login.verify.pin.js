'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.fbid = session.fbid;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {

      var me = this;

      console.log(`parameters ${JSON.stringify(this.parameters)}`);

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to verify PIN
        return new Promise(function(resolve, reject) {
            me.session.context.luka.verifyPIN(me.parameters.phone, me.parameters.pin, function(success){
              //check PIN
              if (!success) {
                console.log(`ERR: PIN invalid for ${me.parameters.phone}`);
                return reject('person-login-verify-pin-retry')
              }
              else {
                console.log('pin verified')
                return resolve();
              }
            });
        });
      })
      .then(function(){
        //try to retrieve person
        return new Promise(function(resolve, reject) {

          me.session.context.luka.getPerson(me.parameters.lukatag, function(profile){
            if (profile.lukatag == undefined) {
              console.log(`ERR: Cannot retrieve ${me.parameters.lukatag}`);
              return reject('person-login-invalid-lukatag')
            }
            else {
              console.log('got person')
                return resolve(profile);
              }
          })
        });
      })
      .then(function(profile){
        //PIN valid
        console.log(`User ${me.parameters.lukatag} validated.`);

        //set authenticated flag
        me.luka.authenticated = true;
        me.luka.profile = profile;
        console.log(`Profile is ${JSON.stringify(profile)}`);
        //add a channel to person profile
        var channel = { type: "facebook", value: me.session.fbid, isVerified: true, metadata: { fbid: me.session.fbpageid}}
        console.log(`Adding channel ${JSON.stringify(channel)}`);
        //get phone
        var phone = "";
        console.log(`Finding a phone..`);
        for (var c=0;c<profile.contacts.length;c++) {
          if (profile.contacts[c].type=="phone") {
            phone = profile.contacts[c].value;
          }
        }
        me.luka.createChannel(me.parameters.lukatag, channel, function(result){
          if (result.response == false) {
            return reject('error');
          }
          else {
            console.log(`Added channel ${JSON.stringify(channel)}`);
            callback({
              source: `${me.source}`,
              followupEvent: {
                name: `person-login-confirm`,
                data: { phone: phone, lukatag: me.parameters.lukatag}
              }
            });

          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        }
       );
      });



    }
}
module.exports = ActionObject;
