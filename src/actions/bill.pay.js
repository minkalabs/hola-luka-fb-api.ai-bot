'use strict';

var rp = require('request-promise');
var dateFormat = require('dateformat');
const cJSON = require('circular-json')
const config = require('../../app.json')
const BOT_APP_URL = config.env.BOT_APP_URL.value;
const MINKA_API_URL = config.env.MINKA_API_URL.value;
const EMAIL_RECEIPT_TEMPLATE_NUMBER = config.env.EMAIL_RECEIPT_TEMPLATE_NUMBER.value;


class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();
      var data = {};
      var bill = undefined;
      var service = undefined;
      var store = undefined;

      var event = undefined;
      var options = undefined;
      sequence = sequence.then(function() {
      //get bill details
        return new Promise(function(resolve, reject) {
            me.luka.getBill(me.parameters.bill, function(_bill){
              //check result
              if (!_bill) {
                console.log(`ERR: Cannot retrieve bill for filter ${me.parameters.bill}`);
                return reject('error')
              }
              else if (_bill.status == `PAID`) {
                //if bill is already paid exit this and just send messaging_events
                bill = _bill;
                return reject(`already_paid`);
              }
              else {
                console.log(`Pay bill ${JSON.stringify(_bill)}`)
                bill = _bill;
                return resolve();
              }
            });
        });

      })
      .then(function() {
      //get service details
        //try to get services
        console.log(`getting the service`)
        return new Promise(function(resolve, reject) {
            me.luka.getService(bill.metadata.service, function(_service){
              //check result
              if (!_service) {
                console.log(`ERR: Cannot retrieve services for filter ${bill.metadata.service}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_service)}`)
                service = _service;
                return resolve();
            });
        });

      })
      .then(function() {
      //get store details
        //try to get store
        console.log(`getting the store for ${service.org}`)
        return new Promise(function(resolve, reject) {
            me.luka.getStore(bill.metadata.store, function(_store){
              //check result
              if (!_store) {
                console.log(`ERR: Cannot retrieve store for filter ${bill.metadata.store}`);
                return reject('error')
              }
              else
                console.log(`we have it here ${JSON.stringify(_store)}`)
                store = _store;
                return resolve();
            });
        });
      })
      .then(function() {
      //pay the bill

        return new Promise(function(resolve, reject) {

          bill.metadata.paymentMethod = me.parameters.paymentMethod;
          data.data = bill;
          var options = {
              method: 'POST',
              uri: `${BOT_APP_URL}/gateway/pay`,
              body: bill,
              json: true // Automatically stringifies the body to JSON
          };

          rp(options)
            .then(function(_bill){
              console.log(`paying bill ${JSON.stringify(_bill)}`)

              bill = _bill;
              if (bill.metadata.billId == undefined || bill.status == 'DRAFT') {
                //error returning
                me.luka.error = bill.metadata.error;
                throw new Error(me.luka.error);
              }
              else if (bill.status == 'PENDING' || bill.status == 'INVALID') {
                //bill payment failed
                data = {bill: `${bill.metadata.billId}`, service: `${bill.metadata.service}`, store: `${bill.metadata.store}`}
                return reject('bill-pay-fail');
                //return resolve();
              }
              else {
                //return result
                return resolve();
                }
              })
            .catch(function(err){
              console.log(`Error paying bill ${JSON.stringify(err)}`);
              return reject('error');
            });


        });
      })
      .then(function() {
      //save bill
        return new Promise(function(resolve, reject) {
          console.log(`verifying payBill ${bill.metadata.billId}... /transfer/${bill._id}/authorize`)
          me.luka.verifyTransaction(bill._id, function(success){
            if (!success) {
              options = {sessionId: `${me.session.sessionId}`};
              event = {name: `error`,data: {}};
              me.bot.addRequestEvents(event,options,me.session);
            }
            else {

              console.log("Displaying final success");
              event = undefined;
              options = undefined;

              options = {sessionId: `${me.session.sessionId}`};
              //if a bill has a service define finish it as Service.Pay.Success and offer to subscribe, if it has a subscription do Subscription.Pay.Success don't offer subscribe, otherwise finish as Bil.Pay.Success
              if (bill.metadata.accountId == undefined && bill.metadata.service == undefined) {
                event = {name: `bill-pay-success`,data: {bill: `${bill.metadata.billId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`}};
                me.bot.addRequestEvents(event,options,me.session);
              }
              else if (bill.metadata.accountId == '' || bill.metadata.service == '') {
                event = {name: `bill-pay-success`,data: {bill: `${bill.metadata.billId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`}};
                me.bot.addRequestEvents(event,options,me.session);
              }
              else if (bill.metadata.accountId != '' && bill.metadata.service != '' && bill.metadata.subscription == '') {
                event = {name: `service-pay-success`,data: {bill: `${bill.metadata.accountId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`, account: `${bill.metadata.accountId}`, service: `${bill.metadata.service}`}};
                me.bot.addRequestEvents(event,options,me.session);
              }
              else if (bill.metadata.accountId != '' && bill.metadata.service != '' && bill.metadata.subscription != '') {
                event = {name: `subscription-pay-success`,data: {bill: `${bill.metadata.accountId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`, account: `${bill.metadata.accountId}`, subscription: `${bill.metadata.subscription}`}};
                me.bot.addRequestEvents(event,options,me.session);
              }
              return resolve();
            }
          })
        });
      })
      .then(function() {
      //send email
        return new Promise(function(resolve, reject) {
          var to = "";
          var subject = `Recibo de pago de ${store.name} # ${bill.metadata.billId}`;
          var body = `${bill.metadata.billId}`;
          for (var c=0;c<me.luka.profile.contacts.length;c++) {
            if (me.luka.profile.contacts[c].type == "email") {
              to = me.luka.profile.contacts[c].value;
            }
          }
          console.log(`sending email to ${to}`)

          var data = {
        		to: `${to}`,
        		subject: `${subject}`,
        		textBody: ``,
            htmlBody: ``,
            metadata: {
                templateId: `${EMAIL_RECEIPT_TEMPLATE_NUMBER}`,
                date: `${bill.metadata.date}`,
                billid: `${bill.metadata.billId}`,
                accountid: `${bill.metadata.accountId}`,
                store: `${store.name}`,
                currency: `${bill.currency}`,
                amount: `${bill.value}`
            }
        	}

          me.luka.sendEmail(data, function(response){
            if (response.error) {
              console.log(`Email sending error: ${response.error.name}`)
            }
            else {
              console.log(`Email sent`)
            }
          })
        });
      })
      .catch(function(followUpEvent){
        console.log(`rejecting ${followUpEvent} ${JSON.stringify(data)}`)
        if (followUpEvent != `already_paid`) {
          options = {sessionId: `${me.session.sessionId}`};
          event = {name: `${followUpEvent}`,data: {}};
          me.bot.addRequestEvents(event,options,me.session);
        }
        else {
          
          console.log("Displaying final success for already paid bill");
          event = undefined;
          options = undefined;

          options = {sessionId: `${me.session.sessionId}`};
          //if a bill has a service define finish it as Service.Pay.Success and offer to subscribe, if it has a subscription do Subscription.Pay.Success don't offer subscribe, otherwise finish as Bil.Pay.Success
          if (bill.metadata.accountId == undefined && bill.metadata.service == undefined) {
            event = {name: `bill-pay-success`,data: {bill: `${bill.metadata.billId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`}};
            me.bot.addRequestEvents(event,options,me.session);
          }
          else if (bill.metadata.accountId == '' || bill.metadata.service == '') {
            event = {name: `bill-pay-success`,data: {bill: `${bill.metadata.billId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`}};
            me.bot.addRequestEvents(event,options,me.session);
          }
          else if (bill.metadata.accountId != '' && bill.metadata.service != '' && bill.metadata.subscription == '') {
            event = {name: `service-pay-success`,data: {bill: `${bill.metadata.accountId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`, account: `${bill.metadata.accountId}`, service: `${bill.metadata.service}`}};
            me.bot.addRequestEvents(event,options,me.session);
          }
          else if (bill.metadata.accountId != '' && bill.metadata.service != '' && bill.metadata.subscription != '') {
            event = {name: `subscription-pay-success`,data: {bill: `${bill.metadata.accountId}`, to: `${bill.destination}`, amount: `${bill.value} ${bill.currency}`, date: `${dateFormat(bill.metadata.updated,'dd.mm.yyyy hh:mm')}`, logo: `${BOT_APP_URL}/img/luka_icon_small.png`, account: `${bill.metadata.accountId}`, subscription: `${bill.metadata.subscription}`}};
            me.bot.addRequestEvents(event,options,me.session);
          }

        }
      });
    }


}

module.exports = ActionObject;
