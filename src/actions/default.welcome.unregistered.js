'use strict';
const util = require('util');

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      var event = undefined;
      var options = undefined;

        var output = {source: `${me.source}`,data: {facebook: [] }}
        //prepare cards for payment options
        output.data.facebook.push({
          attachment: {
            type: "template",
            payload: {
              template_type: "generic",
                elements: [
                  {
                    title: "Si es un usuario nuevo, debe registrarse.",
                    subtitle: "",
                    buttons: [
                      {
                        type:"web_url",
                        title:"Registrarme",
                        url:"https://holaluka-bot.herokuapp.com/signup.html",
                        webview_height_ratio: "full",
                        messenger_extensions: "true",
                        fallback_url: "https://holaluka-bot.herokuapp.com/signup.html"
                      },
                      {
                        title: `Tengo un @lukatag`,
                        type: "postback",
                        payload: `login`
                      }
                    ]
                  }
                ]
            }
          }
          });

          return callback(output);
    }
}

module.exports = ActionObject;
