'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated
      if (!me.luka.authenticated) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'not-authenticated',
            data: {}
          }
        });
      }

      var rejectData = undefined;
      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to find receiver
        return new Promise(function(resolve, reject) {
            me.luka.getPerson(me.parameters.to, function(result){
              //check result
              if (!result) {
                console.log(`ERR: Cannot find receiver ${me.parameters.to}`);
                rejectData = {};
                return reject('person-transfer-invalid-lukatag')
              }
              else
                return resolve();
            });
        });
      })
      .then(function(){
        //try to get senders ("my") balance
        return new Promise(function(resolve, reject) {
            me.luka.getBalance(me.luka.profile.lukatag, function(result){
              //check result
              if (!result) {
                console.log(`ERR: Cannot retrieve balance for ${me.luka.profile.lukatag}`);
                return reject('error')
              }
              else
                return resolve(result);
            });
        });
      })
      .then(function(balance) {
        return new Promise(function(resolve, reject) {
          //get balance from minka api
          var luka_balance = ''; //string containing balance in all currencies: 12 LUK, 2 BOB
          // var balance_in_currency = 0;
          // for (var i=0;i<balance.length;i++) {
          //     if (balance[i].name == me.parameters.currency) {
          //       balance_in_currency = balance[i].qty;
          //     }
          // }
          luka_balance = `${balance.balance} ${me.parameters.currency}`;
          //do I have enugh to send
          if (balance.balance>=me.parameters.amount) {
            return resolve(true);
          }
          else {
            rejectData = { balance: `${luka_balance}` };
            return reject('person-balance-response');
          }
        })
      })
      .then(function(success){
        //try to send payment
        return new Promise(function(resolve, reject) {
            me.luka.sendPayment(me.luka.profile.lukatag, me.parameters.to, me.parameters.amount, me.parameters.currency, me.parameters.note, function(txid){
              //check result
              if (!txid) {
                console.log(`ERR: Cannot make payment to ${me.parameters.to}`);
                return reject('person-transfer-reject')
              }
              else
                return resolve(txid);
            });
        });
      })
      .then(function(txid){
        //try to get senders ("my") balance
        return new Promise(function(resolve, reject) {
            me.luka.verifyTransaction(txid, function(result){
              //check result
              if (!txid) {
                console.log(`ERR: Cannot verify txid ${txid} payment to ${me.parameters.to}`);
                return reject('person-transfer-reject')
              }
              else
                return resolve();
            });
        });
      })
      .then(function(){
        return callback({
          source: `${me.source}`,
          followupEvent: {
            name: `person-transfer-confirm`,
            data: {  }
          }
        });
      })
      .catch(function(followUpEvent){
        return callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: rejectData
          }
        });
      });
    }
}
module.exports = ActionObject;
