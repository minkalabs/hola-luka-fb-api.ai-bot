'use strict';

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
    }
    //main method
    execute(callback) {
      var me = this;
      //check if user is authenticated and if it is admin
      if (!me.luka.authenticated && !me.session.is_admin) {
        callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'input.unknown',
            data: {}
          }
        });
      }

      var sequence = Promise.resolve();

      sequence = sequence.then(function(){
        //try to get balance
        return new Promise(function(resolve, reject) {
            me.luka.getBalance(me.parameters.lukatag, function(balance){
              //check result
              if (!balance) {
                console.log(`ERR: Cannot retrieve balance for ${me.luka.profile.lukatag}`);
                return reject('error')
              }
              else
                console.log(`balance ${JSON.stringify(balance)} ${balance}`);
                var luka_balance = ''; //string containing balance in all currencies: 12 LUK, 2 BOB
                for (var i=0;i<balance.length;i++) {
                    luka_balance += `${balance[i].qty} ${balance[i].name}, `;
                }
                luka_balance = luka_balance.substring(0,luka_balance.length-2);
                console.log(`luka balance ${JSON.stringify(luka_balance)} ${luka_balance}`);
                return resolve(luka_balance);
            });
        });
      })
      .then(function(luka_balance){
        console.log(JSON.stringify(luka_balance));
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: 'person-balance-response',
            data: { balance: `${luka_balance}` }
          }
        });
      })
      .catch(function(followUpEvent){
        callback({
          source: `${me.source}`,
          followupEvent: {
            name: `${followUpEvent}`,
            data: { }
          }
        });
      });
    }
}
module.exports = ActionObject;
