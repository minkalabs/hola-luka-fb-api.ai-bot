'use strict';
const util = require('util');
const cJSON = require('circular-json')

class ActionObject {
    constructor(actionIncomplete, parameters, contexts, intentName, session, source, bot) {

        //API.AI objects
        this.actionIncomplete = actionIncomplete;
        this.parameters = parameters;
        this.contexts = contexts;
        this.intentName = intentName;
        this.session = session;
        this.source = source;
        this.luka = session.context.luka;
        this.bot = bot;
    }
    //main method
    execute(callback) {
      var me = this;
      var event = undefined;
      var options = undefined;

      //check if user is authenticated
      if (!me.luka.authenticated) {

        options = {sessionId: `${me.session.sessionId}`};
        event = {name: 'default-welcome-anonymous',data: {}};
        me.bot.addRequestEvents(event,options,me.session);

        event = undefined;
        options = undefined;

        setTimeout(function(){
          options = {sessionId: `${me.session.sessionId}`};
          event = {name: 'default-welcome-unregistered',data: {}};
          me.bot.addRequestEvents(event,options,me.session);

        }, 3000)


      }
      else {
        console.log(`luka profile is ${cJSON.stringify(me.luka.profile)}`);
        return callback({
          source: `${this.source}`,
          followupEvent: {
            name: 'default-welcome-registered',
            data: { firstname: me.luka.profile.name[0].first, lastname: me.luka.profile.name[0].last }
          }
        });
      }
    }
}

module.exports = ActionObject;
