//Minka adapter for payment gateways
//implements 4 endpoints
// /gateway/in - cash in
// /gateway/out - cash out
// /gateway/verify - verify bill
// /gateway/pay - pay bill

'use strict';

const Minka = require('./minka_api.js');

var S = require('string');
const _ = require('underscore')
const i18n = require('i18n');

class gateway_adapter {

  constructor () {

    this.bill = undefined;
    this.subscription = undefined;
    this.minka = new Minka();

  }

  //cash-in method
  in(id) {

  }

  //cash-out method
  out(id) {

  }

  //verify bill method
  verify(bill, cb) {

    var bill = JSON.parse(bill);
    this.bill = bill;
    console.log(`adapter verify() ${JSON.stringify(bill)}`)
    //verify bill
    this.verifyBill(function(processed_bill){
      cb(processed_bill);
    })

  }

  //pay bill method
  pay(bill, cb) {


    var bill = JSON.parse(bill);
    this.bill = bill;
    console.log(`adapter pay() ${bill}`)
    var gateway = this;
    //pay bill
    this.payBill(function(processed_bill){

      if (processed_bill.status == 'PAID') {
        //if no error, create minka/blockchain transaction
          console.log(`Success processing payBill`)
          cb(processed_bill);
        }
        else {
          console.log(`Error processing payBill`)
          cb(processed_bill);
        }
    });
  }

  //PRIVATE functions
  prepopulateBill(cb) {

    var bill = this.bill;
    bill.metadata.billId = `${new Date().getTime()}-${Math.floor(Math.random()*1000)}`;
    console.log(`prepoluate bill ${JSON.stringify(bill)} ${bill.metadata.service}`);
    var _service = this.minka.getService(bill.metadata.service, function(service){
      console.log(`got service form Minka ${JSON.stringify(service)} ${service.metadata.gateway.name}`);
      bill.metadata.name = service.metadata.name;
      bill.metadata.internalServiceId = service.metadata.gateway.param_1;
      bill.metadata.internalStoreId = service.metadata.gateway.param_2;
      bill.metadata.paymentGateway = service.metadata.gateway.name;
      bill.metadata.paymentMethods = service.metadata.payment_methods;
      bill.metadata.description = service.metadata.description;
      var d = new Date();
      bill.metadata.date = d.toLocaleString();
      if (bill.status == undefined) bill.status = 'DRAFT';
      bill.created = new Date().toISOString();
      console.log(`returning prepopulated bill ${JSON.stringify(bill)}`);
      cb(bill);
    });


  }

  verifyBill(cb) {

    var bill = this.bill;
    var minka = this.minka;
    console.log(`into verify bill`);
    //check if it is a new bill. If TRUE then prepoluate it
    this.prepopulateBill(function(bill){
      console.log(`let's find gateway`);
      //switch case paymentGateway
      if (bill.metadata.paymentGateway == "SINTESIS") {
        //payment gateways
        const Adapter_sintesis = require('./gateway_adapter_sintesis.js');
        var adapter_sintesis = new Adapter_sintesis();
        //get gateway object and initialize gateway adapter
        console.log(`let's get Sintesis gateway`);
        minka.getGateway(bill.metadata.paymentGateway, function(gateway){
            console.log(`got it! gateway is ${JSON.stringify(gateway)}`);
            adapter_sintesis.gateway = gateway[0];
            bill.status = "PENDING";
            console.log(`let's finally verify the bill`);
            adapter_sintesis.verifyBill(bill, function(processed_bill) {
              cb(processed_bill)
            });
        });
      }
    });
  }

  getPaymentURL(bill, cb) {

    var minka = this.minka;
    console.log(`into getPaymentURL for a bill ${JSON.stringify(bill)}`);
    //check if it is a new bill. If TRUE then prepoluate it
    minka.getPerson(bill.source, function(payer){
      console.log(`payer is ${JSON.stringify(payer)}`);
      //switch case paymentGateway
      if (bill.metadata.paymentGateway == "SINTESIS") {
        //payment gateways
        const Adapter_sintesis = require('./gateway_adapter_sintesis.js');
        var adapter_sintesis = new Adapter_sintesis();
        //get gateway object and initialize gateway adapter
        console.log(`let's get Sintesis gateway`);
        minka.getGateway(bill.metadata.paymentGateway, function(gateway){
            console.log(`got it! gateway is ${JSON.stringify(gateway)}`);
            adapter_sintesis.gateway = gateway[0];
            console.log(`let's finally get the payment_url`);
            adapter_sintesis.getPaymentURL(payer, bill, function(payment_url) {
              cb(payment_url)
            });
        });
      }
    });
  }

  payBill(cb) {

    var bill = this.bill;
    var minka = this.minka;
    console.log(`into pay bill`);
    //switch case paymentGateway
    if (bill.metadata.paymentGateway == "SINTESIS") {
      //payment gateways
      const Adapter_sintesis = require('./gateway_adapter_sintesis.js');
      var adapter_sintesis = new Adapter_sintesis();
      //get gateway object and initialize gateway adapter
      minka.getGateway(bill.metadata.paymentGateway, function(gateway){
          console.log(`got gateway! gateway is ${JSON.stringify(gateway)}`);
          adapter_sintesis.gateway = gateway[0];
          bill.status = "PENDING";
          adapter_sintesis.payBill(bill, function(processed_bill) {
            cb(processed_bill)
          });
      });
    }
  }

}

module.exports = gateway_adapter;
