'use strict';
//Minka API middleware

var request=require('request');
var rp=require('request-promise');
var S = require('string');
const _ = require('underscore')
const i18n = require('i18n');
const config = require('../app.json')

//auth security token
const access_token = config.env.MINKA_ACCESS_TOKEN.value;
//Minka API url
const minka_api_url = config.env.MINKA_API_URL.value;
//Minka API url
const bot_app_url = config.env.BOT_APP_URL.value;


class Luka {

  constructor () {

    //is user authenticated?
    this.authenticated = false;

    //FB sender id
    this.sender_id = '';

    //error message
    this.error = '';

    //authenticated Luka user profile
    //lukatag without @
    this.lukatag = '';
    //person/:id profile object
    this.profile = undefined;
    //person/:id/balance object
    this.balance = undefined;

    //Luka profile to Send/Recive amount
    //reciever
    this.payment_profile = undefined;
    this.payment_amount = 0;
    this.payment_currency = "BOB";
    this.payment_note = '';

    this.payment_service_id = '';
    this.payment_account_id = '';

    //Luka service User account number
    this.payer_account_number = '';

    //Luka txid
    this.payment_txid = '';
    //Luka transaction verify code
    this.payment_passcode = '';
    //number of login attempts
    this.login_attempt = 1;

    //FB page link
    this.chatbot_FB_url = 'http://bit.ly/2pbWKCm';

    //FB bot object
    this.bot = undefined;

    //states
    this.previous_step = -1;
    this.current_step = 0;

  }

  //generic POST function;
  //used for Minka API calls not implemente din this class
  genericPost (method, uri, body, cb) {

    var luka = this;

    var options = {
        method: `${method}`,
        uri: `${minka_api_url}${uri}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: body,
        json: true // Automatically stringifies the body to JSON
    };
    console.log(`genericPost ${method} ${minka_api_url}${uri} ${JSON.stringify(body)}`)
    rp(options)
      .then(function(result){
        result = JSON.parse(JSON.stringify(result));
        if (result._id == undefined) {
          //error returning
          luka.error = result.message;
          throw new Error(luka.error);
        }
        else {
          //return result
          return cb(true);
          }
        })
      .catch(function(err){
      console.log(`genericPostError ${err}`)
      return cb(false);
      });

  };

  //authenticate function
  getAccessToken (lukatag, passcode, cb) {

    var luka = this;

    var options = {
        method: 'POST',
        uri: `${minka_api_url}/person/${lukatag}/login`,
        body: {
          passcode:`${passcode}`
        },
        json: true // Automatically stringifies the body to JSON
    };

    rp(options)
      .then(function(body){
        console.log(`getAccessToken ${JSON.stringify(body)}`)
        body = JSON.parse(JSON.stringify(body));
        if (body.token == undefined) {
          //user id or password don't match
          luka.error = body.message;
          throw new Error(luka.error);
        }
        else {
          //user logged in
          console.log(`auth success: ${body.token}`);
          //return access_token
          return cb(body.token);
          }
        })
      .catch(function(err){

        console.log(`${luka.error}`);
        return cb(false);
      });

  };

  //get receive/send person/merchant object
  initPayment (lukatag, cb) {

    var luka = this;

    lukatag = S(lukatag).toLowerCase();
    if (S(lukatag).startsWith('@')) lukatag = S(lukatag).toLowerCase().substr(1,lukatag.length);

    var options = {
        uri: `${minka_api_url}/person/${lukatag}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };

    rp(options)
      .then(function(body){
        luka.payment_profile = JSON.parse(JSON.stringify(body));
        return cb(true);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
      });
  };

  //getBalance function
    getBalance (lukatag, cb) {

      var luka = this;
      //if (!S(lukatag).startsWith('@')) lukatag = `@${lukatag}`;
      lukatag = S(lukatag).toLowerCase();

      var options = {
          uri: `${minka_api_url}/person/${lukatag}/balance?unit=@lco`,
          headers: {
            'x-access-token':`${access_token}`
          },
          json: true // Automatically stringifies the body to JSON
      };

      rp(options)
        .then(function(body){
          //luka.balance = JSON.parse(JSON.stringify(body));
          return cb(JSON.parse(JSON.stringify(body)));
        })
        .catch(function(err){
          luka.error = err;
          console.log(`${luka.error}`);
          return cb(false);
        });
    };

      //initiates a transfer; creates a Minka transaction
      //trasnaction is sent from luka.profile -> luka.payment_profile
    sendPayment(from, to, amount, currency, note, cb) {

      var luka = this;
      console.log(`from ${from} to ${to}`)
      if (!S(from).startsWith('@')) from = `@${from}`;
      if (!S(to).startsWith('@')) to = `@${to}`;

      console.log({
            data: {
             destination: `${to}`,
             source: `${from}`,
             currency: `${currency}`,
             value: `${amount}`,
             charges: {
               txt: ""
             },
             metadata: {
             description: `${note}`
             }
           }
        })

      var options = {
          method: 'POST',
          uri: `${minka_api_url}/transfer`,
          headers: {
            'x-access-token':`${access_token}`
          },
          body: {
                data: {
                 destination: `${to}`,
                 source: `${from}`,
                 currency: `${currency}`,
                 value: `${amount}`,
                 charges: {
                   txt: ""
                 },
                 metadata: {
                 description: `${note}`
                 }
               }
            },
          json: true // Automatically stringifies the body to JSON
      };

      rp(options)
        .then(function(body){
          body = JSON.parse(JSON.stringify(body));
          console.log(`trxId ${body.trxId} ${JSON.stringify(body)}`)
          return cb(body.trxId);
        })
        .catch(function(err){
          luka.error = err;
          console.log(`${luka.error}`);
          return cb(false);
        });

    };

    //initiates a transfer; creates a Minka transaction
    //trasnaction is sent from luka.profile -> luka.payment_profile
  createTransaction(data, cb) {

    var luka = this;
    console.log(`Creating a transaction ${JSON.stringify(data)}`);

    var options = {
        method: 'POST',
        uri: `${minka_api_url}/transfer`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: data,
        json: true // Automatically stringifies the body to JSON
    };

    rp(options)
      .then(function(body){
        body = JSON.parse(JSON.stringify(body));
        console.log(`trxId ${body.trxId} ${JSON.stringify(body)}`)
        return cb(body.trxId);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`Error creating a transaction. ${luka.error}`);
        return cb(false);
      });

  };


    //verifies current transaction
    verifyTransaction(txid,cb) {

      var luka = this;

      var options = {
          method: 'POST',
          uri: `${minka_api_url}/transfer/${txid}/authorize`,
          headers: {
            'x-access-token':`${access_token}`
          },
          body: {
            id : `${txid}`
          },
          json: true // Automatically stringifies the body to JSON
      };

      rp(options)
        .then(function(body){
          return cb(true);
        })
        .catch(function(err){
          luka.error = err;
          console.log(`${luka.error}`);
          return cb(false);
        });
    }

  //verifies PIN to SMS
  verifyPIN(phone,pin,cb) {

    var luka = this;
    //fix
    //return cb(true);
    console.log(`Verifying phone and PIN ${phone} ${pin}`)
    var options = {
        method: 'POST',
        uri: `${minka_api_url}/oauth/login`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: { data: {
          pin: `${pin}`,
          phone: `${phone}`,
          type: `person`
        }},
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        if (JSON.parse(JSON.stringify(body)).token == undefined) {
          return cb(false)
        }
        else {
          return cb(true)
        }
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
      });

  }

  //send a verification PIN using SMS
  sendPIN(phone, cb) {

    var luka = this;
    //FIX
    //return cb(true);
    var options = {
        method: 'POST',
        uri: `${minka_api_url}/channel/sms/pin`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: { data:
          {
            phone : `${phone}`
          }
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        if (JSON.parse(JSON.stringify(body)).message == undefined) {
          return cb(false)
        }
        else {
          return cb(JSON.parse(JSON.stringify(body)).message)
        }
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
     });
   }

  //send a message to the user using SMS
  sendSmsMessage(to_phone,message_body,cb) {

    var luka = this;

    var options = {
        method: 'POST',
        uri: `${minka_api_url}/channel/sms/text`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: {
          phone : `${to_phone}`,
          message : `${message_body}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        return cb(true)
      })
      .catch(function(err){
        luka.error = err;
        console.log(`Error sending SMS ${luka.error}`);
        return cb(false);
     });
  }

  //send a message to the user using SMS
  sendEmail(data,cb) {

    var luka = this;
    console.log(`sending email: ${JSON.stringify(data)}`);
    var options = {
        method: 'POST',
        uri: `${minka_api_url}/channel/email`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: {
        	data: data
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        return cb(true)
      })
      .catch(function(err){
        luka.error = err;
        console.log(`Error sending email ${luka.error}`);
        return cb(false);
     });
  }


  //get a list of all transactions
  getTransactions(cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/person/${luka.lukatag}/transfer`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var luka_transfers = body;
        return cb(luka_transfers)
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });
  }

  //get a list of pending transactions
  getPendingTransactions(lukatag, cb) {

    var luka = this;
    lukatag = S(lukatag).toLowerCase();
    if (S(lukatag).startsWith('@')) lukatag = S(lukatag).toLowerCase().substr(1,lukatag.length);

    console.log(`getPending ${lukatag}`)

    var options = {
        uri: `${minka_api_url}/person/${lukatag}/transfer`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    console.log(`pendingOpts ${JSON.stringify(options)}`);
    rp(options)
      .then(function(body){
        var luka_transfers = _.filter(_.sortBy(body,'created'),function(row){return row.status=='PENDING' && row._links.source==`@${lukatag}`}).reverse();
        return cb(luka_transfers);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });
  }


  //get a transactions
  getTransaction(txid,cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/transfer/${txid}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var luka_transfers = body;
        return cb(luka_transfers)
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });
  }

  //save person profile
  savePerson(userid, phone, lukatag, email, firstname, lastname, address, city, state, zip, country, countryCode, fbid, cb) {

    var luka = this;


console.log(JSON.stringify({
        data: {
          firstname: firstname,
           lastname: lastname,
           lukatag: lukatag,
           phone: phone,
           email: email,
           passcode: "1234",
           identification : [{
             numberId: userid,
             typeId: "ID",
             dateIssue: "1234",
             locationIssue: {
                city: city,
                country: country,
                zip: zip,
                state: state,
                countryCode: countryCode
            },
            verified: "not verified"
            }],
           location: [{
            city: city,
            country: country,
            countryCode: countryCode,
            address: address
          }],
           terms: {
             channel: "facebook",
             verified: "VERIFIED"
           },
          channel: [
            {
              type: "facebook",
              name: "Facebook",
              id: fbid,
              status: "VERIFIED"
            }
          ]
        }
      }))

    var options = {
      method: 'POST',
      uri: `${minka_api_url}/person`,
      headers: {
        'x-access-token':`${access_token}`
      },
      body: {
        data: {
          firstname: firstname,
           lastname: lastname,
           lukatag: lukatag,
           phone: phone,
           email: email,
           passcode: "1234",
           identification : [{
             numberId: userid,
             typeId: "typeId",
             dateIssue: "1234",
             locationIssue: {
                city: city,
                country: country,
                countryCode: countryCode
            },
            verified: "not verified"
            }],
           location: [{
            city: city,
            country: country,
            countryCode: countryCode,
            zip: zip,
            state: state,
            address: address
          }],
           terms: {
             channel: "channel",
             verified: "verified"
           },
          channel: [
            {
              type: "facebook",
              name: "Facebook",
              id: fbid,
              status: "VERIFIED"
            }
          ]
        }
      },
      json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        console.log(`Minka API returned: ${JSON.stringify(body)}`);
        return cb(true)
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
     });
  }

  //get person profile
  getPerson(lukatag, cb) {

    var luka = this;
    var person = undefined;
    //if (!S(lukatag).startsWith('@')) lukatag = `@${lukatag}`;
    lukatag = S(lukatag).toLowerCase();

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/person/${lukatag}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        if (body == undefined || body.error != undefined)
          throw new Error(`Invalid lukatag ${lukatag}`);
        var person = JSON.parse(JSON.stringify(body));
        return cb(person);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });
  }


  //get person profile
  getPersons(cb) {

    var luka = this;
    var persons = undefined;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/person`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var persons = JSON.parse(JSON.stringify(body));
        return cb(persons);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });
  }

  //create channel
  createChannel (lukatag,data, cb) {

    var luka = this;
    if (!S(lukatag).startsWith('@')) lukatag = `@${lukatag}`;
    lukatag = S(lukatag).toLowerCase();
    var options = {
        method: `POST`,
        uri: `${minka_api_url}/person/${lukatag}/contact`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: {
          data: data
        },
        json: true // Automatically stringifies the body to JSON
    };
    console.log(`create channel ${minka_api_url}/person/${lukatag}/contact ${JSON.stringify(data)}`)
    rp(options)
      .then(function(result){
        result = JSON.parse(JSON.stringify(result));
          return cb(true);
        })
      .catch(function(err){
      console.log(`createChannelError ${err}`)
      return cb(false);
      });

  };

  //delete channel
  deleteChannel (person,id, cb) {

    var luka = this;

    var options = {
        method: `DELETE`,
        uri: `${minka_api_url}/person/${person}/contact/${id}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: {},
        json: true // Automatically stringifies the body to JSON
    };
    console.log(`deleteChannel ${minka_api_url}/person/${person}/contact/${id}`)
    rp(options)
      .then(function(result){
          //return result
          console.log(`deleted`);
          return cb(true);
        })
      .catch(function(err){
        console.log(`deleteChannelError ${err}`)
        return cb(false);
      });

  };

  //get a list of services
  getServices(filter,cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/item`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var services = JSON.parse(JSON.stringify(body));
        console.log(`We have services ${services.length}`);
        //FIX until no company, countr: services = JSON.parse(JSON.stringify(_.filter(_.sortBy(services,'name'),function(row){return row._id == `${filter}` || S(row.metadata.company).toLowerCase().startsWith(`${filter}`) || S(row.metadata.name).toLowerCase().startsWith(`${filter}`) || S(row.metadata.lukatag).toLowerCase().startsWith(`${filter}`) || S(row.metadata.country).toLowerCase().startsWith(`${filter}`)})));
        //services = JSON.parse(JSON.stringify(_.filter(services,function(row){return row._id == `${filter}`})));
        console.log(`After we have services ${services.length}`);
        return cb(services);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });


  }
  //get a service by serviceId
  getService(serviceId,cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/item`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var services = JSON.parse(JSON.stringify(body));
        services = JSON.parse(JSON.stringify(_.filter(_.sortBy(services,'storeId'),function(row){return row._id==`${serviceId}`})));
        return cb(services[0]);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });

  }
  //get a gateway by gateway name
  getGateway(name,cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/gateway`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var gateway = JSON.parse(JSON.stringify(body));
        gateway = JSON.parse(JSON.stringify(_.filter(_.sortBy(gateway,'_id'),function(row){return row.name==`${name}`})));
        return cb(gateway);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });



  }
  //get a store by store @lukatag or storeId
  getStore(storeId,cb) {
    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/org`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var store = JSON.parse(JSON.stringify(body));
        store = JSON.parse(JSON.stringify(_.filter(_.sortBy(store,'storeId'),function(row){return row.lukatag==`${storeId}` || row._id==`${storeId}`})));
        return cb(store[0]);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });

  }

  //get a list of stores
  getStores(filter,cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/org`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var stores = JSON.parse(JSON.stringify(body));
        stores = JSON.parse(JSON.stringify(_.filter(_.sortBy(stores,'storeId'),function(row){return row.name == `${filter}` || S(row.store).toLowerCase().startsWith(`${filter}`) || S(row.name).toLowerCase().startsWith(`${filter}`) || S(row.lukatag).toLowerCase().startsWith(`${filter}`)})));
        return cb(stores);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });


  }

  //get a bill by billId or _id
  getBill(trxId,cb) {
    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/transfer/${trxId}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var bill = JSON.parse(JSON.stringify(body));
        //bill = JSON.parse(JSON.stringify(_.filter(_.sortBy(bill,'billId'),function(row){return row.billId==`${billId}` || row._id==`${billId}`})));
        return cb(bill);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });

  }

  //save bill
  saveBill (uri, body, cb) {

    var luka = this;

    var options = {
        method: `POST`,
        uri: `${minka_api_url}${uri}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: body,
        json: true // Automatically stringifies the body to JSON
    };
    console.log(`saveBill ${minka_api_url}${uri} ${JSON.stringify(body)}`)
    rp(options)
      .then(function(result){
        result = JSON.parse(JSON.stringify(result));
        if (result.message != "Transaction executed") {
          //error returning
          luka.error = result.message;
          throw new Error(luka.error);
        }
        else {
          //return result
          return cb(true);
          }
        })
      .catch(function(err){
      console.log(`saveBillError ${err}`)
      return cb(false);
      });

  };

  //get a subscription by subscriptionId
  getSubscription(subscriptionId,cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/contract/${subscriptionId}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var subscription = JSON.parse(JSON.stringify(body));
        return cb(subscription);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });

  }
  //get a service by serviceId
  getPersonSubscriptions(personId, cb) {

    var luka = this;

    var options = {
        method: 'GET',
        uri: `${minka_api_url}/contract`,
        headers: {
          'x-access-token':`${access_token}`
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
      .then(function(body){
        var subscriptions = JSON.parse(JSON.stringify(body));
        subscriptions = JSON.parse(JSON.stringify(_.filter(subscriptions,function(row){return row.person==`${personId}`})));
        return cb(subscriptions);
      })
      .catch(function(err){
        luka.error = err;
        console.log(`${luka.error}`);
        return cb(false);
    });

  }

  //save subscription
  createSubscription (store, service, account, person, name, cb) {

    var luka = this;

    var options = {
        method: `POST`,
        uri: `${minka_api_url}/contract`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: {
              data: {
            		person: person,
            		metadata: {
                  item: service,
            			org: store,
                  accountId: account,
                  name: name,
                  type: `SUBSCRIPTION`,
                  status: `ACTIVE`
              }
	         }
        },
        json: true // Automatically stringifies the body to JSON
    };

    rp(options)
      .then(function(result){

        if (result._id == undefined) {
          //error returning
          luka.error = result._message;
          throw new Error(luka.error);
        }
        else {
          //return result
          console.log(`Contract created ${result._id}`)
          return cb(true);
          }
        })
      .catch(function(err){
      console.log(`createSubscriptionError ${err}`)
      return cb(false);
      });

  }

  //delete subscription
  deleteSubscription (subscription, cb) {

    var luka = this;

    var options = {
        method: `DELETE`,
        uri: `${minka_api_url}/contract/${subscription}`,
        headers: {
          'x-access-token':`${access_token}`
        },
        body: {},
        json: true // Automatically stringifies the body to JSON
    };

    rp(options)
      .then(function(result){

          return cb(true);

        })
      .catch(function(err){
      console.log(`deleteSubscriptionError ${err}`)
      return cb(false);
      });

  }


}
module.exports = Luka;
